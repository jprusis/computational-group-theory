package project.findingloops;

import java.util.LinkedList;
import java.util.ListIterator;

import project.composites.CompositeEdge;
import project.composites.CompositeGraph;
import project.graphs.Edge;
import project.permutations.PermutationGraph;

public class CompositeLoops {
	
	private int[] parentTable;

	public int numLoops(CompositeGraph graph) {
		int numLoops=0;
		int numVertices = graph.enumerateVertices();
		parentTable = new int[numVertices];
		for (int i=0; i<numVertices;i++) {parentTable[i]=i;}
		LinkedList<CompositeEdge> edges = graph.getEdges();
		ListIterator<CompositeEdge> iterator = edges.listIterator();
		CompositeEdge currentEdge;
		while (iterator.hasNext()) {
			currentEdge = iterator.next();
			int v1 = graph.getSlots().getVertex(currentEdge.getStart()).getNum();
			int v2 = graph.getSlots().getVertex(currentEdge.getEnd()).getNum();
			if (returnParent(v1)==returnParent(v2)) {numLoops++;}
			else {parentTable[v1]=v2;}
		}
		
		return numLoops;
	}
	
	private int returnParent(int i) {
		if (i==parentTable[i]) return parentTable[i];
		else return returnParent(parentTable[i]);
	}
	
}
