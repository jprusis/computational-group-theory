package project.findingloops;

import java.util.ArrayList;
import java.util.HashSet;
import java.util.Hashtable;
import java.util.LinkedList;
import java.util.ListIterator;
import java.util.Set;

import javax.script.ScriptEngine;
import javax.script.ScriptEngineManager;
import javax.script.ScriptException;

import project.graphs.Edge;
import project.graphs.Graph;
import project.graphs.Vertex;
import project.permutations.PermutationGraph;
import project.userinput.Attributes;

public class Loops {
	

	private int[] parentTable;
	private Hashtable<Set<LinkedList<Edge>>, String> table;
	private ArrayList<Edge> legs;
	private Set<LinkedList<Edge>> setForTable;
	private boolean isZero;
	

	public Loops(Attributes givenAttributes) {
		legs = givenAttributes.getLegs();
		table = givenAttributes.getTable();
	}
	
	public void findLoops(Graph graph) {
			isZero = false;
			setForTable = new HashSet<LinkedList<Edge>>();
			if (legs==null) {
			LinkedList<Edge> nullList = new LinkedList<Edge>();
			setForTable.add(nullList);
			}
			
			int power = numLoops(graph) - legLoops(graph);
			
			if (!isZero) {
			graph.setScalar(graph.getScalar()*Math.pow(10, power));
			
			ScriptEngineManager scriptManager = new ScriptEngineManager();
		    ScriptEngine engine_1 = scriptManager.getEngineByExtension("m");
		    
		    String powerSt = "" + power; 
			String input = graph.getScalar2() + "*n^" + powerSt;
			String output = "";
			try {
				output = (String) engine_1.eval(input);
			} catch (ScriptException e) {
				e.printStackTrace();
			}
			graph.setScalar2(output);
			
			String currentPoly;
			if (table.get(setForTable) != null) {currentPoly = table.get(setForTable);}
			else {currentPoly = "";}
			input = currentPoly + "+(" + output + ")";
			try {
				output = (String) engine_1.eval(input);
			} catch (ScriptException e) {
				e.printStackTrace();
			}
			table.put(setForTable, output);
			}
	}
	
	public void findLoops(PermutationGraph graph) {
		isZero = false;
		setForTable = new HashSet<LinkedList<Edge>>();
		if (legs==null) {
		LinkedList<Edge> nullList = new LinkedList<Edge>();
		setForTable.add(nullList);
		}
		
		int power = numLoops(graph);
		
		if (!isZero) {
		graph.setScalar(graph.getScalar()*Math.pow(10, power));
		
		ScriptEngineManager scriptManager = new ScriptEngineManager();
	    ScriptEngine engine_1 = scriptManager.getEngineByExtension("m");
	    
	    String powerSt = "" + power; 
		String input = graph.getScalar2() + "*n^" + powerSt;
		String output = "";
		try {
			output = (String) engine_1.eval(input);
		} catch (ScriptException e) {
			e.printStackTrace();
		}
		graph.setScalar2(output);
		
		String currentPoly;
		if (table.get(setForTable) != null) {currentPoly = table.get(setForTable);}
		else {currentPoly = "";}
		input = currentPoly + "+(" + output + ")";
		try {
			output = (String) engine_1.eval(input);
		} catch (ScriptException e) {
			e.printStackTrace();
		}
		table.put(setForTable, output);
		}
	}
	
	private int legLoops(Graph graph) {
		int legLoops = 0;
		ArrayList<Edge> edges = legs;
		Hashtable<Integer, LinkedList<Edge>> legLoopTable = new Hashtable<Integer, LinkedList<Edge>>();
		ListIterator<Edge> iterator = edges.listIterator();
		Edge currentEdge;
		while (iterator.hasNext()) {
			currentEdge = iterator.next();
				int vertexNum = graph.getSlots().getVertex(currentEdge.getStart()).getNum();
				int parent = returnParent(vertexNum);
				if (legLoopTable.containsKey(parent)==false) {
					LinkedList<Edge> edgeList = new LinkedList<Edge>();
					Edge nextEdge = currentEdge;
					edgeList.add(nextEdge);
					while ((nextEdge = giveNextLegInLoop(nextEdge, graph)) != currentEdge) {
						edgeList.add(nextEdge);
						}
					legLoopTable.put(parent, edgeList);
					setForTable.add(edgeList);
					if (edgeList.size()==1) {isZero = true;}
					legLoops++;
				}
		}
		return legLoops;
	}

	public int numLoops(Graph graph) {
		int numLoops=0;
		int numVertices = graph.enumerateVertices();
		parentTable = new int[numVertices];
		for (int i=0; i<numVertices;i++) {parentTable[i]=i;}
		LinkedList<Edge> edges = graph.getEdges();
		ListIterator<Edge> iterator = edges.listIterator();
		Edge currentEdge;
		while (iterator.hasNext()) {
			currentEdge = iterator.next();
			int v1 = graph.getSlots().getVertex(currentEdge.getStart()).getNum();
			int v2 = graph.getSlots().getVertex(currentEdge.getEnd()).getNum();
			if (returnParent(v1)==returnParent(v2)) {numLoops++;}
			else {parentTable[v1]=v2;}
		}
		
		return numLoops;
	}
	
	public int numLoops(PermutationGraph graph) {
		int numLoops=0;
		int numVertices = graph.enumerateVertices();
		parentTable = new int[numVertices];
		for (int i=0; i<numVertices;i++) {parentTable[i]=i;}
		LinkedList<Edge> edges = graph.getEdges();
		ListIterator<Edge> iterator = edges.listIterator();
		Edge currentEdge;
		while (iterator.hasNext()) {
			currentEdge = iterator.next();
			int v1 = graph.getSlots().getVertex(currentEdge.getStart()).getNum();
			int v2 = graph.getSlots().getVertex(currentEdge.getEnd()).getNum();
			if (returnParent(v1)==returnParent(v2)) {numLoops++;}
			else {parentTable[v1]=v2;}
		}
		
		return numLoops;
	}
	
	private int returnParent(int i) {
		if (i==parentTable[i]) return parentTable[i];
		else return returnParent(parentTable[i]);
	}
	
	private Edge giveNextLegInLoop(Edge givenLeg, Graph graph) {
		Vertex startVertex;
		if (graph.getSlots().getVertex(givenLeg.getStart()).getType() == 1) {
			startVertex = graph.getSlots().getVertex(givenLeg.getStart());
		} else {startVertex=graph.getSlots().getVertex(givenLeg.getEnd());}
		
		Edge nextEdge=null, currentEdge;
		int slot = 0;
		for (int i=1; i<4; i++) {
			slot = startVertex.getSlot(i);
			currentEdge = graph.getSlots().getEdge(slot);
			if (currentEdge != givenLeg) {
				if (currentEdge.getStart() == slot) {
					nextEdge = currentEdge;
					}
			}
		}
		return goLoopUntilLeg(nextEdge, graph);
	}
	
	private Edge goLoopUntilLeg(Edge nextEdge, Graph graph) {
		int slot=0;
		Edge currentEdge;
		Vertex currentVertex = graph.getSlots().getVertex(nextEdge.getEnd());
		for (int i=1; i<4; i++) {
			slot = currentVertex.getSlot(i);
			currentEdge = graph.getSlots().getEdge(slot);
			if (currentEdge != null) {
				if (currentEdge.getType() == 2) {
					return currentEdge;
				}
				if (currentEdge.getStart() == slot) {
					nextEdge = currentEdge;
					}
			}
		}
		return goLoopUntilLeg(nextEdge, graph);
	}

}
