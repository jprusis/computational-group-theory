package project.reduction;

import project.composites.CompositeGraph;
import project.graphs.Edge;
import project.graphs.Graph;
import project.graphs.GraphList;
import project.graphs.Vertex;
import project.permutations.PermutationGraph;
import project.userinput.Attributes;

/**
 * @author  janis
 */
public class GraphReducer {
	
	/**
	 * @uml.property  name="graphs"
	 * @uml.associationEnd  
	 */
	private GraphList graphs;
	private CompositeGraph comGraph;

	public void reduce(Attributes givenAttributes) {
		
		if (givenAttributes.getMethod()==0) {
			graphs = givenAttributes.getGraphs();
			if (givenAttributes.getGroup()==0) { 
				System.out.println("Given group: SU(n)");
				reduceGGGVertices();
				reduceQGQVerticesSUN();
			} else if (givenAttributes.getGroup()==1) {
				System.out.println("Given group: SO(n)");
				reduceGGGVertices();
				reduceQGQVerticesSON();
			} else {
				System.err.println("Error: given group type not recognised");
			}
		} else if (givenAttributes.getMethod()==1) {
			graphs = givenAttributes.getGraphs();
			reduceAntisymmetrizers();
			reduceSymmetrizers();
		} else {
			comGraph = givenAttributes.getGivenComp();
			System.out.println("Given method: Composite");
			reduceComposite();
		}
		
		
	}

	private void reduceComposite() {
		boolean check=true;
		while (check) {
			
			check = false;
			
			if (comGraph.reduceCycles()) {
				check=true;
			}
			else if (comGraph.reduce6js()) {
				check=true;
			}
			else if (comGraph.recouple()) {
				check=true;
			}
			
		}
	}

	private void reduceSymmetrizers() {
		graphs.createIterator();
		while (graphs.checkNextGraph()) {
			PermutationGraph graph = graphs.getNextPermutationGraph();
			if (graph.anySymmetrizers()) {
				PermutationGraph[] results = graph.PermuteSymmetrizer();
				graphs.removeCurrent();
				for (int i=0; i<results.length; i++) {
					graphs.addToIterator(results[i]);
				}
				graphs.createIterator();
			}
		}
	}
	
	private void reduceAntisymmetrizers() {
		graphs.createIterator();
		while (graphs.checkNextGraph()) {
			PermutationGraph graph = graphs.getNextPermutationGraph();
			if (graph.anyAntisymmetrizers()) {
				PermutationGraph[] results = graph.PermuteAntisymmetrizer();
				graphs.removeCurrent();
				for (int i=0; i<results.length; i++) {
					graphs.addToIterator(results[i]);
				}
				graphs.createIterator();
			}
		}
	}

	private void printGraphs() {
		Vertex currentVertex;
		Edge currentEdge;
		for (int k =0; k<graphs.getSize(); k++) {
			Graph graph = graphs.getElement(k);
			for (int i = 0; i<graph.getEdges().size(); i++) {
				currentEdge = graph.getEdges().get(i);
				System.out.println(currentEdge.getStart() + " to " + currentEdge.getEnd()+ " type: " + currentEdge.getType());
			}
			for (int i = 0; i<graph.getVertices().size(); i++) {
				currentVertex = graph.getVertices().get(i);
				System.out.println(currentVertex.getSlot1() + " " + currentVertex.getSlot2() + " " + currentVertex.getSlot3() + " type:" + currentVertex.getType());
			}
			System.out.println("-----------------------------------------");
		}
	}

	private void reduceQGQVerticesSON() {
		System.out.println("Reducing qgq vertices..");
		graphs.createIterator();
		while (graphs.checkNextGraph()) {
			Graph graph = graphs.getNextGraph();
			int i = 0, check=0;
			while ((i<graph.getVertices().size()) && (check==0)) {
				if (graph.getVertices().get(i).getType()==1) {
					Vertex otherVertex = graph.findVertex(i);
					if (otherVertex != null) {
						Graph graph1 = graph.reduceQGQ_SONReturnGraph1(i);
						Graph graph2 = graph.reduceQGQ_SONReturnGraph2(i);
						graphs.removeCurrent();
						graphs.addToIterator(graph1);
						graphs.addToIterator(graph2);
						graphs.createIterator();
						check=1;
					}
				}
				i++;
			}
		}	
	}

	private void reduceQGQVerticesSUN() {
		System.out.println("Reducing qgq vertices..");
		graphs.createIterator();
		while (graphs.checkNextGraph()) {
			Graph graph = graphs.getNextGraph();
			int i = 0, check=0;
			while ((i<graph.getVertices().size()) && (check==0)) {
				if (graph.getVertices().get(i).getType()==1) {
					Vertex otherVertex = graph.findVertex(i);
					if (otherVertex != null) {
						Graph graph1 = graph.reduceQGQ_SUNReturnGraph1(i);
						Graph graph2 = graph.reduceQGQ_SUNReturnGraph2(i);
						graphs.removeCurrent();
						graphs.addToIterator(graph1);
						graphs.addToIterator(graph2);
						graphs.createIterator();
						check=1;
					}
				}
				i++;
			}
		}
	}

	private void reduceGGGVertices() {
		System.out.println("Reducing ggg vertices..");
		graphs.createIterator();
		while (graphs.checkNextGraph()) {
			Graph graph = graphs.getNextGraph();
			int i = 0, check=0;
			while ((i<graph.getVertices().size()) && (check==0)) {
				if (graph.getVertices().get(i).getType()==2) {
					Graph graph1 = graph.insertPositiveGGG(i);
					Graph graph2 = graph.insertNegativeGGG(i);
					graphs.removeCurrent();
					graphs.addToIterator(graph1);
					graphs.addToIterator(graph2);
					graphs.createIterator();
					check=1;
				}
				i++;
			}	
		}
	}

}
