package project.composites;


import java.util.Hashtable;
import java.util.LinkedList;

public class CompositeSlots {

		private Hashtable<Integer, CompositeVertex> verticeTable;
		private Hashtable<Integer, CompositeEdge> edgeTable;

		public CompositeSlots(LinkedList<CompositeEdge> givenEdges, LinkedList<CompositeVertex> givenVertices) {
			Hashtable<Integer, CompositeEdge> edges = new Hashtable<Integer, CompositeEdge>();
			Hashtable<Integer, CompositeVertex> vertices = new Hashtable<Integer, CompositeVertex>();
			for (int i =0; i< givenEdges.size(); i++) {
				edges.put(givenEdges.get(i).getStart(), givenEdges.get(i));
				edges.put(givenEdges.get(i).getEnd(), givenEdges.get(i));
			}
			for (int i =0; i< givenVertices.size(); i++) {
				vertices.put(givenVertices.get(i).getSlot1(), givenVertices.get(i));
				vertices.put(givenVertices.get(i).getSlot2(), givenVertices.get(i));
				vertices.put(givenVertices.get(i).getSlot3(), givenVertices.get(i));
			}
			edgeTable = edges;
			verticeTable = vertices;
		}
		

		public CompositeEdge getEdge(int i) {
			return edgeTable.get(i);
		}
		
		public CompositeVertex getVertex(int i) {
			return verticeTable.get(i);
		}
		
		public int getNumElements() {
			int slotsVertices = verticeTable.size();
			int slotsEdges = edgeTable.size();
			if (slotsVertices>slotsEdges) {return slotsVertices;}
			else return slotsEdges;
		}
		
		public void putVertex(int slot, CompositeVertex givenVertex) {
			verticeTable.put(slot, givenVertex);
		}
		
		public void putEdge(int slot, CompositeEdge givenEdge) {
			edgeTable.put(slot, givenEdge);
		}
		
		public void removeVertex(int key) {
			verticeTable.remove(key);
		}

	}
