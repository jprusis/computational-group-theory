package project.composites;

/**
 * @author  janis
 */
public class Tableau {
	
	/**
	 * @uml.property  name="structure"
	 */
	protected int[] structure;
	/**
	 * @uml.property  name="numBoxes"
	 */
	protected int numBoxes;

	public Tableau(int[] structure) {
		this.structure = structure;
		recalculateBoxes();
	}

	private void recalculateBoxes() {
		numBoxes = 0;
		for (int i=0; i<structure.length; i++) {
			numBoxes += structure[i];
		}
	}

	/**
	 * @return
	 * @uml.property  name="structure"
	 */
	public int[] getStructure() {
		return structure;
	}

	/**
	 * @param structure
	 * @uml.property  name="structure"
	 */
	public void setStructure(int[] structure) {
		this.structure = structure;
	}

	/**
	 * @return
	 * @uml.property  name="numBoxes"
	 */
	public int getNumBoxes() {
		return numBoxes;
	}

	/**
	 * @param numBoxes
	 * @uml.property  name="numBoxes"
	 */
	public void setNumBoxes(int numBoxes) {
		this.numBoxes = numBoxes;
	}
	
	public int integerDimension(int givenN) {
		int dim=1;
		for(int i=0; i<structure.length; i++) {
			for(int k=(givenN-i); k<(givenN-i+structure[i]);k++){
				dim *=k;
			}
		}
		return dim/hook();
	}
	
	public int[] coefficient() {
		int multiple = 1;
		for(int i=0; i<structure.length; i++) {
			multiple = multiple * factorial(structure[i]);
		}
		int[] inverse = invertTab(structure);
		for(int i=0; i<inverse.length; i++) {
			multiple = multiple * factorial(inverse[i]);
		}
		int[] result = {multiple, hook()};
		return result;
	}
	
	public int hook() {
		int hook=1;
		int[][] matrix = new int[structure.length][structure[0]];
		for(int i=structure.length-1; i>=0; i--) {
			for(int j=0; j<structure[0]; j++) {
				if (j<structure[i]) {
					matrix[i][j] = structure[i] - j;
				}
				for (int k=i+1; k<structure.length;k++) {
					if (matrix[k][j] != 0) {
						matrix[i][j] ++;
					}
				}
			}
		}
		for(int i=0; i<structure.length; i++) {
			for(int j=0; j<structure[0]; j++) {
				if (matrix[i][j]!=0) {
					hook *= matrix[i][j];
				}
			}
		}
		return hook;
	}
	
	private int[] invertTab(int[] structure) {
		int[] result = new int[structure[0]];
		int[][] matrix = new int[structure.length][structure[0]];
		for (int i=0;i<structure.length;i++) {
			for (int j=0; j<structure[i]; j++) {
				matrix[i][j] = 1;
			}
		}
		int counter;
		for (int i=0; i<structure[0]; i++) {
			counter =0;
			for (int j=0;j<structure.length;j++) {
				if (matrix[j][i]==1) {counter++;}
			}
			result[i]=counter;
		}
		return result;
	}
	
	private static int factorial(int n) {
        int fact = 1; 
        for (int i = 1; i <= n; i++) {
            fact *= i;
        }
        return fact;
    }
}
