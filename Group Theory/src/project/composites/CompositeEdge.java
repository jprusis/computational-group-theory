package project.composites;

import java.util.LinkedList;

import project.permutations.Antisymmetrizer;
import project.permutations.Symmetrizer;

/**
 * @author  janis
 */
public class CompositeEdge {

	/**
	 * @uml.property  name="start"
	 */
	protected int start;
	/**
	 * @uml.property  name="end"
	 */
	protected int end;
	/**
	 * @uml.property  name="numLines"
	 */
	protected int numLines;
	protected boolean isSimple;
	private int num;
	private LinkedList<Symmetrizer> sym; 
	private LinkedList<Antisymmetrizer> anti;
	/**
	 * @uml.property  name="tableau"
	 * @uml.associationEnd  
	 */
	protected Tableau tableau;
	
	public CompositeEdge(int start, int end, Tableau tableau) {
		this.start = start;
		this.end = end;
		this.tableau = tableau;
		numLines = tableau.getNumBoxes();
		isSimple = true;
	}

	/**
	 * @return
	 * @uml.property  name="start"
	 */
	public int getStart() {
		return start;
	}

	/**
	 * @param start
	 * @uml.property  name="start"
	 */
	public void setStart(int start) {
		this.start = start;
	}

	/**
	 * @return
	 * @uml.property  name="end"
	 */
	public int getEnd() {
		return end;
	}

	/**
	 * @param end
	 * @uml.property  name="end"
	 */
	public void setEnd(int end) {
		this.end = end;
	}

	/**
	 * @return
	 * @uml.property  name="numLines"
	 */
	public int getNumLines() {
		return numLines;
	}

	/**
	 * @param numLines
	 * @uml.property  name="numLines"
	 */
	public void setNumLines(int numLines) {
		this.numLines = numLines;
	}

	/**
	 * @return
	 * @uml.property  name="tableau"
	 */
	public Tableau getTableau() {
		return tableau;
	}

	/**
	 * @param tableau
	 * @uml.property  name="tableau"
	 */
	public void setTableau(Tableau tableau) {
		this.tableau = tableau;
	}

	public boolean isSimple() {
		return isSimple;
	}

	public void setSimple(boolean isSimple) {
		this.isSimple = isSimple;
	}

	public int getNum() {
		return num;
	}

	public void setNum(int num) {
		this.num = num;
	}

	public LinkedList<Symmetrizer> getSym() {
		return sym;
	}

	public void setSym(LinkedList<Symmetrizer> sym) {
		this.sym = sym;
	}

	public LinkedList<Antisymmetrizer> getAnti() {
		return anti;
	}

	public void setAnti(LinkedList<Antisymmetrizer> anti) {
		this.anti = anti;
	}
	
	
	
}
