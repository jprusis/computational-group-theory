package project.composites;

import java.util.HashSet;
import java.util.LinkedList;
import java.util.ListIterator;
import java.util.Set;

import javax.script.ScriptEngine;
import javax.script.ScriptEngineManager;
import javax.script.ScriptException;

import project.graphs.SlotList;
import project.graphs.Vertex;

public class CompositeGraph {

	private LinkedList<CompositeEdge> edges;
	private LinkedList<CompositeVertex> vertices;
	private CompositeSlots slots;
	private double scalar;
	private String scalar2;
	private int[] parentTable;
	private int depth, dim;
	private boolean four;
	private Set<Integer> fourSet;
	
	public CompositeGraph(LinkedList<CompositeEdge> edges, LinkedList<CompositeVertex> vertices, CompositeSlots slots, int dim) {
		this.edges = edges;
		this.vertices = vertices;
		this.slots = slots;
		scalar = 1;
		scalar2 = "1";
		this.dim = dim;
	}
	
	public int enumerateVertices() {
		int num=0;
		ListIterator<CompositeVertex> iterator = vertices.listIterator();
		while (iterator.hasNext()) {
			CompositeVertex current = iterator.next();
			current.setNum(num);
			num++;
		}
		return num;
		
	}
	
	public int enumerateEdges() {
		int num=1;
		ListIterator<CompositeEdge> iterator = edges.listIterator();
		while (iterator.hasNext()) {
			CompositeEdge current = iterator.next();
			current.setNum(num);
			num++;
		}
		return num;
		
	}
	
	public boolean reduceCycles() {
		CompositeEdge current, compare, out1=null, out2=null;
		CompositeVertex start, end;
		int whereNow;
		int size = edges.size();
		ListIterator<CompositeEdge> iterator = edges.listIterator();
		while (iterator.hasNext()) {
			current = iterator.next();
			start = slots.getVertex(current.getStart());
			end = slots.getVertex(current.getEnd());
			whereNow = edges.indexOf(current);
			for (int i = whereNow+1; i<size; i++) {
				compare = edges.get(i);
				if (current != compare) {
				if (((slots.getVertex(compare.getStart()) == end) && (slots.getVertex(compare.getEnd()) == start)) || ((slots.getVertex(compare.getStart()) == start) && (slots.getVertex(compare.getEnd()) == end))) {
					
					for (int k=1;k<=3;k++) {
						if ((start.getSlot(k)!=compare.getStart()) && (start.getSlot(k)!=compare.getEnd()) && (start.getSlot(k)!=current.getStart()) && (start.getSlot(k)!=current.getEnd()) ) {
							out1 = slots.getEdge(start.getSlot(k));
						}
						if ((end.getSlot(k)!=compare.getStart()) && (end.getSlot(k)!=compare.getEnd()) && (end.getSlot(k)!=current.getStart()) && (end.getSlot(k)!=current.getEnd()) ) {
							out2 = slots.getEdge(end.getSlot(k));
						}
					}
					
					if (out1!=out2) {
						if (slots.getVertex(out1.getEnd())==start) {
							out1.setEnd(out2.getEnd());
							slots.putEdge(out1.getEnd(), out1);
						} else {
							out1.setStart(out2.getStart());
							slots.putEdge(out1.getStart(), out1);
						}
					}
					
					
					Recouplings rem3j = new Recouplings();
					String multiple = rem3j.get3j(current, compare, out1, start, end);
					
					ScriptEngineManager scriptManager = new ScriptEngineManager();
				    ScriptEngine engine_1 = scriptManager.getEngineByExtension("m");
					
					String input = "(" + multiple + ")*(" + scalar2 + ")";
					String output = "";
					try {
						output = (String) engine_1.eval(input);
					} catch (ScriptException e) {
						e.printStackTrace();
					}
					
					scalar2 = output;
					
					
					edges.remove(out2);
					edges.remove(current);
					edges.remove(compare);
					vertices.remove(start);
					vertices.remove(end);
					
					
					return true;
				}
				}
			}
		}
		return false;
	}
	
	
	public boolean reduce6js() {
		
		four=false;
		if ((numLoops()>=2) && (four)) {
			if (thereIs6j()) {
				return true;
			}
		}	
		return false;
	}

	private boolean thereIs6j() {
		
		int[] localSet = new int[4];
		int i = 0;
		int checkEnds, numBoth=0, outsideEdgeNumber=0;
		for (Integer val : fourSet) localSet[i++] = val;
		
		CompositeVertex[] fourVerteces = new CompositeVertex[4];
		CompositeEdge[] outsideEdges = new CompositeEdge[2];
		LinkedList<CompositeEdge> listEdges = new LinkedList<CompositeEdge>();
		ListIterator<CompositeVertex> vertexIterator = vertices.listIterator();
		CompositeVertex currVertex;
		int countVert =0;
		while (vertexIterator.hasNext()) {
			currVertex = vertexIterator.next();
			for (i=0;i<4;i++) {
				if (currVertex.getNum()==localSet[i]) {
					fourVerteces[countVert] = currVertex;
					countVert++;
				}
			}
		}
		CompositeVertex start, end;
		ListIterator<CompositeEdge> iterator = edges.listIterator();
		CompositeEdge currentEdge;
		while (iterator.hasNext()) {
			currentEdge = iterator.next();
			checkEnds=0;
			start = slots.getVertex(currentEdge.getStart());
			end = slots.getVertex(currentEdge.getEnd());
			if (start != null) {
				for (i=0;i<4;i++) {
					if (fourVerteces[i] == start) {
						checkEnds++;
					}
				}
			}
			if (end != null) {
				for (i=0;i<4;i++) {
					if (fourVerteces[i] == end) {
						checkEnds++;
					}
				}
			}
			if (checkEnds>0) {
				if (checkEnds==2) {
					listEdges.add(currentEdge);
					numBoth++;
					}
				if (checkEnds==1) {
					outsideEdges[outsideEdgeNumber] = currentEdge;
					if (outsideEdgeNumber == 0) {outsideEdgeNumber =1;}
					else {outsideEdgeNumber =0;}
				}
			}
		}
		
		if (numBoth>4) {
			
			boolean edgeAdded = false;
			if (outsideEdges[0] == outsideEdges[1]) {
				listEdges.add(outsideEdges[0]);
			} else {
				CompositeVertex begins = slots.getVertex(outsideEdges[0].getStart());
				if (begins != null) {
					for (i=0; i<4; i++) {
						if (fourVerteces[i] == begins) {
							outsideEdges[0].setEnd(outsideEdges[1].getEnd());
							slots.putEdge(outsideEdges[1].getEnd(), outsideEdges[0]);
							edges.remove(outsideEdges[1]);
							listEdges.add(outsideEdges[0]);
							edgeAdded = true;
						}
					}
				}
				if (!edgeAdded) {
					outsideEdges[0].setStart(outsideEdges[1].getStart());
					slots.putEdge(outsideEdges[1].getStart(), outsideEdges[0]);
					edges.remove(outsideEdges[1]);
					listEdges.add(outsideEdges[0]);
					edgeAdded = true;
				}
			}
			
			
			Recouplings rem6j = new Recouplings();
			String multiple = rem6j.get6j(fourVerteces, listEdges);
			
			ScriptEngineManager scriptManager = new ScriptEngineManager();
		    ScriptEngine engine_1 = scriptManager.getEngineByExtension("m");
			
			String input = "(" + multiple + ")*(" + scalar2 + ")";
			String output = "";
			try {
				output = (String) engine_1.eval(input);
			} catch (ScriptException e) {
				e.printStackTrace();
			}
			
			scalar2 = output;
			
			ListIterator<CompositeEdge> iterator2 = listEdges.listIterator();
			while (iterator2.hasNext()) {
				edges.remove(iterator2.next());
			}
			edges.add(outsideEdges[0]);
			for (i=0; i<4; i++) {
				vertices.remove(fourVerteces[i]);
				for (int j=1;j<4;j++) {
					slots.removeVertex(fourVerteces[i].getSlot(j));
				}
			}
			return true;
			
		}
	
		return false;
	}
	
	public int numLoops() {
		int numLoops=0, depth1, depth2;
		int numVertices = enumerateVertices();
		if (numVertices>3) {
		parentTable = new int[numVertices];
		for (int i=0; i<numVertices;i++) {parentTable[i]=i;}
		ListIterator<CompositeEdge> iterator = edges.listIterator();
		CompositeEdge currentEdge;
		while (iterator.hasNext()) {
			currentEdge = iterator.next();
			if ((slots.getVertex(currentEdge.getStart()) != null) && (slots.getVertex(currentEdge.getEnd()) != null)) {
				int v1 = slots.getVertex(currentEdge.getStart()).getNum();
				int v2 = slots.getVertex(currentEdge.getEnd()).getNum();
				depth=0;
				int parent1 = returnParent(v1);
				depth1=depth;
				depth=0;
				int parent2 = returnParent(v2);
				depth2=depth;
				if (parent1==parent2) {
					numLoops++;
					if ((depth1+depth2-1) == 4) {
						four = true;
						fourSet = new HashSet<Integer>();
						returnFourParent(v1);
						returnFourParent(v2);
					}
				}
				else {parentTable[v1]=v2;}
			}
		
		}
		
		return numLoops;
	}
		return 0;
	}
	
	private int returnParent(int i) {
		depth++;
		if (i==parentTable[i]) return parentTable[i];
		else return returnParent(parentTable[i]);
	}
	
	private int returnFourParent(int i) {
		fourSet.add(i);
		if (i==parentTable[i]) return parentTable[i];
		else return returnFourParent(parentTable[i]);
	}
	
	public int[] giveConnections(int start) {
		CompositeVertex vertex = slots.getVertex(start);
		if (vertex == null) {
			return null;
		}
		else { 
			int[] result = new int[2]; 
			int counter =0;
			for(int i=1;i<4;i++) {
				if (vertex.getSlot(i) != start) {
					result[counter] = slots.getEdge(vertex.getSlot(i)).getNum();
					counter++;
				}
			}
			return result;
		}
	}
	
	public boolean recouple() {
		
		LinkedList<CompositeEdge> legs = new LinkedList<CompositeEdge>();
		LinkedList<CompositeVertex> legVertices = new LinkedList<CompositeVertex>();
		ListIterator<CompositeEdge> iterator = edges.listIterator();
		CompositeEdge currentEdge;
		while (iterator.hasNext()) {
			currentEdge = iterator.next();
			if ((slots.getVertex(currentEdge.getStart())==null) || (slots.getVertex(currentEdge.getEnd())==null)) {
				legs.add(currentEdge);
				if (slots.getVertex(currentEdge.getStart())!=null) {
					legVertices.add(slots.getVertex(currentEdge.getStart()));
				}
				if (slots.getVertex(currentEdge.getEnd())!=null) {
					legVertices.add(slots.getVertex(currentEdge.getEnd()));
				}
			}
		}
		ListIterator<CompositeEdge> iterator2 = edges.listIterator();
		int counter, sign=1;
		CompositeVertex[] twoVertices = new CompositeVertex[2];
		while (iterator.hasNext()) {
			currentEdge = iterator.next();
			ListIterator<CompositeVertex> iteratorVertex = legVertices.listIterator();
			CompositeVertex currentVertex;
			counter=0;
			while (iterator.hasNext()) {
				currentVertex = iteratorVertex.next();
				if ((slots.getVertex(currentEdge.getStart())==currentVertex) || (slots.getVertex(currentEdge.getEnd())==currentVertex)) {
					twoVertices[counter] = currentVertex;
					counter++;
				}
			}
			if (counter==2) {
				CompositeEdge edge1=null, edge2=null;
				for (int i=1; i<4; i++) {
					if ((twoVertices[0].getSlot(i)!= currentEdge.getStart()) && (twoVertices[0].getSlot(i)!= currentEdge.getEnd()) && (!legs.contains(slots.getEdge(twoVertices[0].getSlot(i))))) {
						edge1 = slots.getEdge(twoVertices[0].getSlot(i));
						if (slots.getVertex(edge1.getStart())==twoVertices[0]) {sign = -1;}
					}
					if ((twoVertices[1].getSlot(i)!= currentEdge.getStart()) && (twoVertices[1].getSlot(i)!= currentEdge.getEnd()) && (!legs.contains(slots.getEdge(twoVertices[1].getSlot(i))))) {
						edge2 = slots.getEdge(twoVertices[1].getSlot(i));
						if (slots.getVertex(edge2.getStart())==twoVertices[1]) {sign = sign * (-1);}
					}
				}
				sign = sign * (-1);
				Recouplings recouple = new Recouplings();
				recouple.recouple(edge1, edge2, sign);
				return true;
			}
		}
		
		return false;
	}
	
	public int getDim() {
		return dim;
	}

	public void setDim(int dim) {
		this.dim = dim;
	}

	public LinkedList<CompositeEdge> getEdges() {
		return edges;
	}

	public void setEdges(LinkedList<CompositeEdge> edges) {
		this.edges = edges;
	}

	public LinkedList<CompositeVertex> getVertices() {
		return vertices;
	}

	public void setVertices(LinkedList<CompositeVertex> vertices) {
		this.vertices = vertices;
	}

	public CompositeSlots getSlots() {
		return slots;
	}

	public void setSlots(CompositeSlots slots) {
		this.slots = slots;
	}

	public double getScalar() {
		return scalar;
	}

	public void setScalar(double scalar) {
		this.scalar = scalar;
	}

	public String getScalar2() {
		return scalar2;
	}

	public void setScalar2(String scalar2) {
		this.scalar2 = scalar2;
	}
	
	
}
