package project.composites;

import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.LinkedList;
import java.util.List;
import java.util.ListIterator;
import java.util.Set;

import javax.script.ScriptEngine;
import javax.script.ScriptEngineManager;

import project.findingloops.Loops;
import project.graphs.Edge;
import project.graphs.GraphList;
import project.graphs.SlotList;
import project.graphs.Vertex;
import project.output.GenerateOutput;
import project.permutations.Antisymmetrizer;
import project.permutations.PermutationGraph;
import project.permutations.Symmetrizer;
import project.reduction.GraphReducer;
import project.userinput.Attributes;

public class Recouplings {
	
	private static ArrayList<ArrayList<Integer>> partitionSet;
	private LinkedList<Symmetrizer> sym; 
	private LinkedList<Antisymmetrizer> anti;
	private LinkedList<Edge> edges;
	private LinkedList<Vertex> vertices;
	private SlotList slots;;
	private int slotNum;
	
	public Recouplings() {
		partitionSet = new ArrayList<ArrayList<Integer>>();
		sym = new LinkedList<Symmetrizer>(); 
		anti = new LinkedList<Antisymmetrizer>();
		edges = new LinkedList<Edge>();
		vertices = new LinkedList<Vertex>();
		slots = new SlotList(edges, vertices, sym, anti);
		slotNum=1;
	}

	public static void partition(int n) {
		ArrayList<Integer> start = new ArrayList<Integer>();
		partition(n, n, start);
	}
	    
	public static void partition(int n, int max, ArrayList<Integer> given) {
		if (n == 0) {
			partitionSet.add(given);
			return;
		}
	  
		for (int i = Math.min(max, n); i >= 1; i--) {
			ArrayList<Integer> addition = new ArrayList<Integer>();
			addition.addAll(given);
			addition.add(i);
			partition(n-i, i, addition);
		}
	}
	
	private void possibilities(Tableau first, Tableau second, int numBoxes) {
		int[] structure = {numBoxes};
		Tableau third = new Tableau(structure);
		giveOrder(first,second,third);
	}
	
	
	public void giveOrder(Tableau first, Tableau second, Tableau third) {
		int num1 = first.getNumBoxes();
		int num2 = second.getNumBoxes();
		int num3 = third.getNumBoxes();
		if ((num1 >= num2) && (num1 >= num3)) {
			Tableau exchange = first;
			first = third;
			third = exchange;
		} else if ((num2 >= num1) && (num2 >= num3)) {
			Tableau exchange = second;
			second = third;
			third = exchange;
		}
		num1 = first.getNumBoxes();
		num2 = second.getNumBoxes();
		num3 = third.getNumBoxes();
		if (num2>num1) {
			Tableau exchange = first;
			first = second;
			second = exchange;
			num1 = first.getNumBoxes();
			num2 = second.getNumBoxes();
		}
		
	}
	
	public void edgeToSymmetrizers(CompositeEdge givenEdge) {
		Tableau givenTab = givenEdge.getTableau();
		int[] structure = givenTab.getStructure();
		Symmetrizer current;
		Antisymmetrizer current2;
		LinkedList<Symmetrizer> localSym = new LinkedList<Symmetrizer>(); 
		LinkedList<Antisymmetrizer> localAnti = new LinkedList<Antisymmetrizer>();
		int [] inverted = invertTab(structure);
		
		for (int i=0;i<structure.length;i++) {
			int[] inSlots = new int[structure[i]];
			int[] outSlots = new int[structure[i]];
		//	System.out.println("Sym");
			for (int j=0; j<structure[i]; j++) {
		//		System.out.print("in " + slotNum + " ");
				inSlots[j] = slotNum;
				slotNum++;
		//		System.out.print("out " + slotNum + " ");
				outSlots[j] = slotNum;
				slotNum++;
			}
			current = new Symmetrizer(inSlots, outSlots);
			sym.add(current);
			localSym.add(current);
			slots.putSym(current);
		}
		
		for (int i=0;i<inverted.length;i++) {
			int[] inSlots = new int[inverted[i]];
			int[] outSlots = new int[inverted[i]];
		//	System.out.println("Anti");
			for (int j=0; j<inverted[i]; j++) {
		//		System.out.print("in " + slotNum + " ");
				inSlots[j] = slotNum;
				slotNum++;
		//		System.out.print("out " + slotNum + " ");
				outSlots[j] = slotNum;
				slotNum++;
			}
			current2 = new Antisymmetrizer(inSlots, outSlots);
			anti.add(current2);
			localAnti.add(current2);
			slots.putAnti(current2);
		}
		
		ListIterator<Symmetrizer> iterator = localSym.listIterator();
		Edge currentEdge;
		int test=0, end=0;
		
		while (iterator.hasNext()) {
			current = iterator.next();
			for (int j=0;j<current.getSize();j++) {
			test=0;
				current2 = localAnti.get(j);
				for (int k=0; k<current2.getSize();k++) {
					if ((test==0) && (slots.getEdge(current2.getSlotsIn()[k]) == null)) {
						currentEdge = new Edge(current.getSlotsOut()[j], current2.getSlotsIn()[k], 1);
						edges.add(currentEdge);
				//		System.out.println("Edge " + currentEdge.getStart() + " " + currentEdge.getEnd());
						slots.putEdge(currentEdge.getStart(), currentEdge);
						slots.putEdge(currentEdge.getEnd(), currentEdge);
						test=1;
					}	
				}
			}
		}
		givenEdge.setSym(localSym);
		givenEdge.setAnti(localAnti);
	}

	private int[] invertTab(int[] structure) {
		int[] result = new int[structure[0]];
		int[][] matrix = new int[structure.length][structure[0]];
		for (int i=0;i<structure.length;i++) {
			for (int j=0; j<structure[i]; j++) {
				matrix[i][j] = 1;
			}
		}
		int counter;
		for (int i=0; i<structure[0]; i++) {
			counter =0;
			for (int j=0;j<structure.length;j++) {
				if (matrix[j][i]==1) {counter++;}
			}
			result[i]=counter;
		}
		return result;
	}

	public String get3j(CompositeEdge current, CompositeEdge compare, CompositeEdge out, CompositeVertex start, CompositeVertex end) {
		long coefficient[] = {1,1};
		
		edgeToSymmetrizers(compare);
		edgeToSymmetrizers(current);
		edgeToSymmetrizers(out);
		
		if ((current.numLines >= compare.numLines) && (current.numLines>=out.numLines)) {
			combineEdges(compare, out, current);
			combineEdges(compare, out, current);
		} else if (out.numLines >= compare.numLines) {
			combineEdges(compare, current, out);
			combineEdges(compare, current, out);
		} else {
			combineEdges(compare, out, compare);
			combineEdges(compare, out, compare);
		}
		
		
		int[] curr = current.getTableau().coefficient();
		for (int i=0; i<2; i++) {
		coefficient[i] = coefficient[i] * ((long) curr[i]);
		}
		curr = compare.getTableau().coefficient();
		for (int i=0; i<2; i++) {
		coefficient[i] = coefficient[i] * ((long) curr[i]);
		}
		curr = out.getTableau().coefficient();
		for (int i=0; i<2; i++) {
		coefficient[i] = coefficient[i] * ((long) curr[i]);
		}
		double coeff = ((double) coefficient[0])/((double) coefficient[1]);
		String multiple = Long.toString(coefficient[0]) + "/" + Long.toString(coefficient[1]);
		
		ArrayList<Edge> legs = new ArrayList<Edge>();
		PermutationGraph initialGraph = new PermutationGraph(edges, vertices, sym, anti, slots, coeff);
		initialGraph.setScalar2(String.format(multiple));
		GraphList initialGraphList = new GraphList(initialGraph);
		
		Attributes attrib = new Attributes(initialGraphList, 0, 1);
		attrib.setLegs(legs);
		new GraphReducer().reduce(attrib);
		
		Loops looper = new Loops(attrib);
		GraphList graphs = attrib.getGraphs();
		graphs.createIterator();
		while (graphs.checkNextGraph()) {
			PermutationGraph graph = graphs.getNextPermutationGraph();
			looper.findLoops(graph);
		}
	    
		String input = "0";
		Set<Set<LinkedList<Edge>>> keys = attrib.getTable().keySet();
		for(Set<LinkedList<Edge>> key: keys){
			input = attrib.getTable().get(key);
		}
		
		return input;
	}

	public String get6j(CompositeVertex[] fourVerteces, LinkedList<CompositeEdge> listEdges) {
		long coefficient[] = {1,1};
		for (int i=0;i<4;i++) {
			int edgeCounter=0;
			CompositeEdge[] edges = new CompositeEdge[3];
			ListIterator<CompositeEdge> iterator = listEdges.listIterator();
			CompositeEdge currentEdge;
			while (iterator.hasNext()) {
				currentEdge = iterator.next();
				for (int j=1;j<4;j++) {
					if ((fourVerteces[i].getSlot(j) == currentEdge.getStart()) || (fourVerteces[i].getSlot(j) == currentEdge.getEnd())) {
						edges[edgeCounter] = currentEdge;
						edgeCounter++;
					}
				}
			}
			for (int j=0;j<3;j++) {
				if (edges[j].getAnti()==null) {
					edgeToSymmetrizers(edges[j]);
				}
			}
			int largeEdge=0;
			edgeCounter=0;
			for (int j=0;j<3;j++) {
				if (edges[j].getNumLines()>edgeCounter) {
					edgeCounter= edges[j].getNumLines();
					largeEdge=j;
				}
			}
			int one = (largeEdge + 1) % 3; 
			int two = (largeEdge + 2) % 3; 
			combineEdges(edges[one], edges[two], edges[largeEdge]);
		}
		
		ListIterator<CompositeEdge> iterator = listEdges.listIterator();
		while (iterator.hasNext()) {
			int[] curr = iterator.next().getTableau().coefficient();
			for (int i=0; i<2; i++) {
			coefficient[i] = coefficient[i] * ((long) curr[i]);
			}
		}
		double coeff = ((double) coefficient[0])/((double) coefficient[1]);
		String multiple = Long.toString(coefficient[0]) + "/" + Long.toString(coefficient[1]);
		
		ArrayList<Edge> legs = new ArrayList<Edge>();
		PermutationGraph initialGraph = new PermutationGraph(edges, vertices, sym, anti, slots, coeff);
		initialGraph.setScalar2(String.format(multiple));
		GraphList initialGraphList = new GraphList(initialGraph);
		
		Attributes attrib = new Attributes(initialGraphList, 0, 1);
		attrib.setLegs(legs);
		new GraphReducer().reduce(attrib);
		
		Loops looper = new Loops(attrib);
		GraphList graphs = attrib.getGraphs();
		graphs.createIterator();
		while (graphs.checkNextGraph()) {
			PermutationGraph graph = graphs.getNextPermutationGraph();
			looper.findLoops(graph);
		}
	    
		String input = "0";
		Set<Set<LinkedList<Edge>>> keys = attrib.getTable().keySet();
		for(Set<LinkedList<Edge>> key: keys){
			input = attrib.getTable().get(key);
		}
		
		return input;
	}

	private void combineEdges(CompositeEdge edge1, CompositeEdge edge2, CompositeEdge edge3) {
		LinkedList<Symmetrizer> localSym1 = edge1.getSym();
		LinkedList<Antisymmetrizer> localAnti1 = edge1.getAnti();
		LinkedList<Symmetrizer> localSym2 = edge2.getSym();
		LinkedList<Antisymmetrizer> localAnti2 = edge2.getAnti();
		LinkedList<Symmetrizer> localSym3 = edge3.getSym();
		LinkedList<Antisymmetrizer> localAnti3 = edge3.getAnti();
		
		if ((slots.getEdge(localSym3.getFirst().getSlotsIn()[0])==null) && (slots.getEdge(localAnti1.getFirst().getSlotsOut()[0])==null)) {
			
			LinkedList<Antisymmetrizer> localAntiOut = new LinkedList<Antisymmetrizer>();
			
			LinkedList<Antisymmetrizer> localAntiCopy1 = new LinkedList<Antisymmetrizer>();
			LinkedList<Antisymmetrizer> localAntiCopy2 = new LinkedList<Antisymmetrizer>();
			for (int i=0; i<localAnti1.size();i++) {
				localAntiCopy1.add(localAnti1.get(i));
			}
			for (int i=0; i<localAnti2.size();i++) {
				localAntiCopy2.add(localAnti2.get(i));
			}
			edge1.setAnti(localAntiCopy1);
			edge2.setAnti(localAntiCopy2);
			
			while ((!localAnti1.isEmpty()) || (!localAnti2.isEmpty())) {
				
				if ((!localAnti1.isEmpty()) && (!localAnti2.isEmpty())) {
					if (localAnti1.getFirst().getSize()>localAnti2.getFirst().getSize()) {
						localAntiOut.add(localAnti1.getFirst());
						localAnti1.remove();
					} else {
						localAntiOut.add(localAnti2.getFirst());
						localAnti2.remove();
					}
				} else {
					if (!localAnti1.isEmpty()) {
						localAntiOut.add(localAnti1.getFirst());
						localAnti1.remove();
					}
					if (!localAnti2.isEmpty()) {
						localAntiOut.add(localAnti2.getFirst());
						localAnti2.remove();
					}
				}
			}
			
			ListIterator<Antisymmetrizer> iterator = localAntiOut.listIterator();
			Antisymmetrizer current;
			Symmetrizer current2;
			Edge currentEdge;
			int test=0, end=0;
			
			while (iterator.hasNext()) {
				current = iterator.next();
				for (int j=0;j<current.getSize();j++) {
				test=0;
					for (int j2=j;j2<localSym3.size();j2++) {
						current2 = localSym3.get(j2);
						for (int k=0; k<current2.getSize();k++) {
							if ((test==0) && (slots.getEdge(current2.getSlotsIn()[k]) == null)) {
								currentEdge = new Edge(current.getSlotsOut()[j], current2.getSlotsIn()[k], 1);
								edges.add(currentEdge);
						//		System.out.println("Edge " + currentEdge.getStart() + " " + currentEdge.getEnd());
								slots.putEdge(currentEdge.getStart(), currentEdge);
								slots.putEdge(currentEdge.getEnd(), currentEdge);
								test=1;
							}	
						}
					}
				}
			}
			
		} else {
			
			LinkedList<Symmetrizer> localSymIn = new LinkedList<Symmetrizer>();
			
			LinkedList<Symmetrizer> localSymCopy1 = new LinkedList<Symmetrizer>();
			LinkedList<Symmetrizer> localSymCopy2 = new LinkedList<Symmetrizer>();
			for (int i=0; i<localSym1.size();i++) {
				localSymCopy1.add(localSym1.get(i));
			}
			for (int i=0; i<localSym2.size();i++) {
				localSymCopy2.add(localSym2.get(i));
			}
			edge1.setSym(localSymCopy1);
			edge2.setSym(localSymCopy2);
			
			while ((!localSym1.isEmpty()) || (!localSym2.isEmpty())) {
				
				if ((!localSym1.isEmpty()) && (!localSym2.isEmpty())) {
					if (localSym1.getFirst().getSize()>localSym2.getFirst().getSize()) {
						localSymIn.add(localSym1.getFirst());
						localSym1.remove();
					} else {
						localSymIn.add(localSym2.getFirst());
						localSym2.remove();
					}
				} else {
					if (!localSym1.isEmpty()) {
						localSymIn.add(localSym1.getFirst());
						localSym1.remove();
					}
					if (!localSym2.isEmpty()) {
						localSymIn.add(localSym2.getFirst());
						localSym2.remove();
					}
				}
			}
			
			ListIterator<Symmetrizer> iterator = localSymIn.listIterator();
			Symmetrizer current;
			Antisymmetrizer current2;
			Edge currentEdge;
			int test=0, end=0;
			
			while (iterator.hasNext()) {
				current = iterator.next();
				for (int j=0;j<current.getSize();j++) {
				test=0;
					for (int j2=j;j2<localAnti3.size();j2++) {
						current2 = localAnti3.get(j2);
						for (int k=0; k<current2.getSize();k++) {
							if ((test==0) && (slots.getEdge(current2.getSlotsOut()[k]) == null)) {
								currentEdge = new Edge(current2.getSlotsOut()[k], current.getSlotsIn()[j], 1);
								edges.add(currentEdge);
							//	System.out.println("Edge " + currentEdge.getStart() + " " + currentEdge.getEnd());
								slots.putEdge(currentEdge.getStart(), currentEdge);
								slots.putEdge(currentEdge.getEnd(), currentEdge);
								test=1;
							}	
						}
					}
				}
			}
			
		}
		
	}

	public void recouple(CompositeEdge edge1, CompositeEdge edge2, int sign) {
		Tableau first= edge1.getTableau();
		Tableau second= edge2.getTableau();
		int numBoxes = first.getNumBoxes() + sign*second.getNumBoxes();
		if (numBoxes == 0) {
			int exchange = edge1.getEnd();
			edge1.setEnd(edge2.getEnd());
			edge2.setEnd(exchange);
		} else {
			possibilities(first,second,numBoxes);
		}
	}
	
}
