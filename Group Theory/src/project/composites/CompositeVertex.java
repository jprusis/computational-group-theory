package project.composites;

/**
 * @author  janis
 */
public class CompositeVertex {

	/**
	 * @uml.property  name="slot1"
	 */
	private int slot1;
	/**
	 * @uml.property  name="slot2"
	 */
	private int slot2;
	/**
	 * @uml.property  name="slot3"
	 */
	private int slot3;
	private int num;
	private int permut;
	
	// must add choice of Littlewood-Richardson combination of primitives
	
	public CompositeVertex(int slot1, int slot2, int slot3, int permut) {
		this.slot1 = slot1;
		this.slot2 = slot2;
		this.slot3 = slot3;
		this.permut = permut;
	}
	
	public CompositeVertex(int slot1, int slot2, int slot3) {
		this.slot1 = slot1;
		this.slot2 = slot2;
		this.slot3 = slot3;
	}

	
	
	public int getPermut() {
		return permut;
	}

	public void setPermut(int permut) {
		this.permut = permut;
	}

	/**
	 * @return
	 * @uml.property  name="slot1"
	 */
	public int getSlot(int i) {
		if (i==1) {return slot1;}
		else if (i==2) {return slot2;}
		else {return slot3;}
	}
	
	
	public int getSlot1() {
		return slot1;
	}

	/**
	 * @param slot1
	 * @uml.property  name="slot1"
	 */
	public void setSlot1(int slot1) {
		this.slot1 = slot1;
	}

	/**
	 * @return
	 * @uml.property  name="slot2"
	 */
	public int getSlot2() {
		return slot2;
	}

	/**
	 * @param slot2
	 * @uml.property  name="slot2"
	 */
	public void setSlot2(int slot2) {
		this.slot2 = slot2;
	}

	/**
	 * @return
	 * @uml.property  name="slot3"
	 */
	public int getSlot3() {
		return slot3;
	}

	/**
	 * @param slot3
	 * @uml.property  name="slot3"
	 */
	public void setSlot3(int slot3) {
		this.slot3 = slot3;
	}
	
	public int getNum() {
		return num;
	}

	/**
	 * @param num
	 * @uml.property  name="num"
	 */
	public void setNum(int num) {
		this.num = num;
	}
	
}
