package project.output;

import java.util.ArrayList;
import java.util.LinkedList;
import java.util.ListIterator;
import java.util.Set;

import javax.script.ScriptEngine;
import javax.script.ScriptEngineManager;
import javax.script.ScriptException;

import project.composites.CompositeEdge;
import project.composites.CompositeGraph;
import project.composites.Tableau;
import project.graphs.Edge;
import project.userinput.Attributes;

public class GenerateOutput {
	
	private ArrayList<Edge> legs;

	public void generate(Attributes givenAttributes) {
		System.out.println("Generating output..");
		legs = givenAttributes.getLegs();
		
		ScriptEngineManager scriptManager = new ScriptEngineManager();
	    ScriptEngine engine_1 = scriptManager.getEngineByExtension("m");
	    
		String input = "0";
		Set<Set<LinkedList<Edge>>> keys = givenAttributes.getTable().keySet();
		System.out.println("Results:");
		System.out.println("---------------------------------------------------");
		for(Set<LinkedList<Edge>> key: keys){
			input = givenAttributes.getTable().get(key);
			if (!input.equals("0")) {
			/*	input = "Factor[" + input + "]";
				try {
					input = (String) engine_1.eval(input);
				} catch (ScriptException e) {
					e.printStackTrace();
				} */
				printSet(key);
				System.out.println("  Poly: " + input);
			}
		}
		System.out.println("---------------------------------------------------");
	}
	
	public void generateComposite(Attributes givenAttributes) {
		System.out.println("Generating output..");
		CompositeGraph graph = givenAttributes.getGivenComp();
		String poly = graph.getScalar2();
		LinkedList<CompositeEdge> edges = graph.getEdges();
		ListIterator<CompositeEdge> iterator = edges.listIterator();
		graph.enumerateEdges();
		String buildUp, young, startString, endString;
		int start, end;
		int[] startConnections, endConnections;
		
		System.out.println("Results:");
		System.out.println("---------------------------------------------------");
		System.out.println("Poly: " + poly);
		
		while (iterator.hasNext()) {
			CompositeEdge current = iterator.next();
			String num = Integer.toString(current.getNum());
			Tableau givenTab = current.getTableau();
			int [] structure = givenTab.getStructure();
			young = "|";
			for (int i=0;i<structure.length;i++) {
				young = young + Integer.toString(structure[i]) + "|";
			}
			if (current.isSimple()) {buildUp = "Simple";}
			else {buildUp = "Complex";}
			start = current.getStart();
			end = current.getEnd();
			startConnections = graph.giveConnections(start);
			if (startConnections == null) {startString = "none";}
			else {
				startString = "|" + Integer.toString(startConnections[0]) + "|" + Integer.toString(startConnections[1]) + "|";
			}
			endConnections = graph.giveConnections(end);
			if (endConnections == null) {endString = "none";}
			else {
				endString = "|" + Integer.toString(endConnections[0]) + "|" + Integer.toString(endConnections[1]) + "|";
			}
			System.out.println("Edge: " + num + " # Type: " + buildUp + " # Young: " + young + " # Connected: start: " + startString + " end: " + endString);
		}
		System.out.println("---------------------------------------------------");
	}
	
	private void printSet(Set<LinkedList<Edge>> givenSet) {
		if (!legs.isEmpty()) {
			System.out.print("Legs: |");
		} else {
			System.out.print("No legs!");
		}
		for(LinkedList<Edge> key: givenSet){
			for (Edge current: key) {
				int index = legs.indexOf(current)+1;
				System.out.print(" " + index);
			}
			System.out.print(" |");
		}
	}

}
