package project.permutations;

import java.util.LinkedList;
import java.util.List;
import java.util.ListIterator;

import javax.script.ScriptEngine;
import javax.script.ScriptEngineManager;
import javax.script.ScriptException;

import project.graphs.Edge;
import project.graphs.SlotList;
import project.graphs.Vertex;

/**
 * @author  janis
 */
public class PermutationGraph {
	
	/**
	 * @uml.property  name="edges"
	 */
	private LinkedList<Edge> edges;
	/**
	 * @uml.property  name="vertices"
	 */
	private LinkedList<Vertex> vertices;
	/**
	 * @uml.property  name="symmetrizers"
	 */
	private LinkedList<Symmetrizer> symmetrizers;
	/**
	 * @uml.property  name="antisymmetrizers"
	 */
	private LinkedList<Antisymmetrizer> antisymmetrizers;
	/**
	 * @uml.property  name="slots"
	 * @uml.associationEnd  
	 */
	private SlotList slots;
	/**
	 * @uml.property  name="scalar"
	 */
	private double scalar;
	/**
	 * @uml.property  name="scalar2"
	 */
	private String scalar2;
	
	
	public PermutationGraph(LinkedList<Edge> edges, LinkedList<Vertex> vertices, LinkedList<Symmetrizer> symmetrizers, LinkedList<Antisymmetrizer> antisymmetrizers, SlotList slots, double i) {
		this.edges = edges;
		this.vertices = vertices;
		this.slots = slots;
		this.symmetrizers = symmetrizers;
		this.antisymmetrizers = antisymmetrizers;
		scalar = i;
		scalar2 = "1";
	}

	public PermutationGraph[] PermuteSymmetrizer() {
		Symmetrizer sym = symmetrizers.getFirst();
		int size = sym.getSize();
		int numGraphs = factorial(size);
		PermutationGraph[] graphs = new PermutationGraph[numGraphs];
		int startslot = slots.getNumElements() + 1;
		
		LinkedList<Vertex> newVertices = new LinkedList<Vertex>();
		for (int k=0; k<vertices.size();k++) {
			newVertices.add(vertices.get(k));
		}
		for (int i=0; i<size; i++) {
			Vertex vertex1 = new Vertex(sym.getSlotsIn()[i],startslot + i, startslot + (2*size) +i ,0);
			newVertices.add(vertex1);
			Vertex vertex2 = new Vertex(sym.getSlotsOut()[i],startslot + size + i, startslot + (3*size) +i ,0);
			newVertices.add(vertex2);
		}
		
		ScriptEngineManager scriptManager = new ScriptEngineManager();
	    ScriptEngine engine_1 = scriptManager.getEngineByExtension("m");
	    
		String input = "(" + scalar2 + ")/" + Integer.toString(numGraphs);
		String output = "";
		try {
			output = (String) engine_1.eval(input);
		} catch (ScriptException e) {
			e.printStackTrace();
		}
		
		Permutations perm = new Permutations();
		LinkedList<List<Integer>> permList = perm.giveList(startslot+size, startslot+(2*size));
		ListIterator<List<Integer>> iterator = permList.listIterator();
		int destination;
		int count =0;
		while (iterator.hasNext()) {
			List<Integer> currentList = iterator.next();
			LinkedList<Edge> newEdges = new LinkedList<Edge>();
			for (int k=0; k<edges.size();k++) {
				newEdges.add(edges.get(k));
			}
			LinkedList<Symmetrizer> newSymmetrizers = new LinkedList<Symmetrizer>();
			for (int k=0; k<symmetrizers.size();k++) {
				newSymmetrizers.add(symmetrizers.get(k));
			}
			newSymmetrizers.remove();
			LinkedList<Antisymmetrizer> newAntisymmetrizers = new LinkedList<Antisymmetrizer>();
			for (int k=0; k<antisymmetrizers.size();k++) {
				newAntisymmetrizers.add(antisymmetrizers.get(k));
			}
			
			for (int i=0; i<size; i++) {
				destination = currentList.get(i);
				Edge newEdge = new Edge(startslot + i,destination,1);
				newEdges.add(newEdge);
			}
			SlotList newSlots = new SlotList(newEdges, newVertices, newSymmetrizers, newAntisymmetrizers);
			PermutationGraph newGraph = new PermutationGraph(newEdges,newVertices,newSymmetrizers, newAntisymmetrizers, newSlots, scalar/numGraphs);
			newGraph.setScalar2(output);
			graphs[count] = newGraph;
			count++;
		}
		
		return graphs;
	}
	
	public PermutationGraph[] PermuteAntisymmetrizer() {
		Antisymmetrizer anti = antisymmetrizers.getFirst();
		int size = anti.getSize();
		int numGraphs = factorial(size);
		PermutationGraph[] graphs = new PermutationGraph[numGraphs];
		int startslot = slots.getNumElements() + 1;
		
		LinkedList<Vertex> newVertices = new LinkedList<Vertex>();
		for (int k=0; k<vertices.size();k++) {
			newVertices.add(vertices.get(k));
		}
		for (int i=0; i<size; i++) {
			Vertex vertex1 = new Vertex(anti.getSlotsIn()[i],startslot + i, startslot + (2*size) +i ,0);
			newVertices.add(vertex1);
			Vertex vertex2 = new Vertex(anti.getSlotsOut()[i],startslot + size + i, startslot + (3*size) +i ,0);
			newVertices.add(vertex2);
		}
		
		Permutations perm = new Permutations();
		LinkedList<List<Integer>> permList = perm.giveList(startslot+size, startslot+(2*size));
		ListIterator<List<Integer>> iterator = permList.listIterator();
		int destination;
		int count =0;
		while (iterator.hasNext()) {
			List<Integer> currentList = iterator.next();
			double newScalar = scalar/numGraphs;
			boolean odd = perm.isOdd(currentList, startslot+size);
			if (odd) {newScalar = (-1)*newScalar;}
			
			ScriptEngineManager scriptManager = new ScriptEngineManager();
		    ScriptEngine engine_1 = scriptManager.getEngineByExtension("m");
		    
		    String input;
		    if (odd) {
		    	input = "-(" + scalar2 + ")/" + Integer.toString(numGraphs);
		    } else {
		    	input = "(" + scalar2 + ")/" + Integer.toString(numGraphs);
		    }
			String output = "";
			try {
				output = (String) engine_1.eval(input);
			} catch (ScriptException e) {
				e.printStackTrace();
			}
			
			LinkedList<Edge> newEdges = new LinkedList<Edge>();
			for (int k=0; k<edges.size();k++) {
				newEdges.add(edges.get(k));
			}
			LinkedList<Symmetrizer> newSymmetrizers = new LinkedList<Symmetrizer>();
			for (int k=0; k<symmetrizers.size();k++) {
				newSymmetrizers.add(symmetrizers.get(k));
			}
			LinkedList<Antisymmetrizer> newAntisymmetrizers = new LinkedList<Antisymmetrizer>();
			for (int k=0; k<antisymmetrizers.size();k++) {
				newAntisymmetrizers.add(antisymmetrizers.get(k));
			}
			newAntisymmetrizers.remove();
			
			for (int i=0; i<size; i++) {
				destination = currentList.get(i);
				Edge newEdge = new Edge(startslot + i,destination,1);
				newEdges.add(newEdge);
			}
			SlotList newSlots = new SlotList(newEdges, newVertices, newSymmetrizers, newAntisymmetrizers);
			PermutationGraph newGraph = new PermutationGraph(newEdges,newVertices,newSymmetrizers, newAntisymmetrizers, newSlots, newScalar);
			newGraph.setScalar2(output);
			graphs[count] = newGraph;
			count++;
		}
		
		return graphs;
	}

	public int enumerateVertices() {
		int num=1;
		ListIterator<Vertex> iterator = vertices.listIterator();
		while (iterator.hasNext()) {
			Vertex current = iterator.next();
			current.setNum(num);
			num++;
		}
		return num;
		
	}
	 public boolean anySymmetrizers() {
		 if (symmetrizers.isEmpty()) {
		 return false; } else {
			 return true;
		 }
	 }
	 
	 public boolean anyAntisymmetrizers() {
		 if (antisymmetrizers.isEmpty()) {
		 return false; } else {
			 return true;
		 }
	 }

	private static int factorial(int n) {
        int fact = 1; 
        for (int i = 1; i <= n; i++) {
            fact *= i;
        }
        return fact;
    }
	
	/**
	 * @return
	 * @uml.property  name="edges"
	 */
	public LinkedList<Edge> getEdges() {
		return edges;
	}


	/**
	 * @param edges
	 * @uml.property  name="edges"
	 */
	public void setEdges(LinkedList<Edge> edges) {
		this.edges = edges;
	}


	/**
	 * @return
	 * @uml.property  name="vertices"
	 */
	public LinkedList<Vertex> getVertices() {
		return vertices;
	}


	/**
	 * @param vertices
	 * @uml.property  name="vertices"
	 */
	public void setVertices(LinkedList<Vertex> vertices) {
		this.vertices = vertices;
	}


	/**
	 * @return
	 * @uml.property  name="symmetrizers"
	 */
	public LinkedList<Symmetrizer> getSymmetrizers() {
		return symmetrizers;
	}


	/**
	 * @param symmetrizers
	 * @uml.property  name="symmetrizers"
	 */
	public void setSymmetrizers(LinkedList<Symmetrizer> symmetrizers) {
		this.symmetrizers = symmetrizers;
	}


	/**
	 * @return
	 * @uml.property  name="antisymmetrizers"
	 */
	public LinkedList<Antisymmetrizer> getAntisymmetrizers() {
		return antisymmetrizers;
	}


	/**
	 * @param antisymmetrizers
	 * @uml.property  name="antisymmetrizers"
	 */
	public void setAntisymmetrizers(LinkedList<Antisymmetrizer> antisymmetrizers) {
		this.antisymmetrizers = antisymmetrizers;
	}


	/**
	 * @return
	 * @uml.property  name="slots"
	 */
	public SlotList getSlots() {
		return slots;
	}


	/**
	 * @param slots
	 * @uml.property  name="slots"
	 */
	public void setSlots(SlotList slots) {
		this.slots = slots;
	}


	/**
	 * @return
	 * @uml.property  name="scalar"
	 */
	public double getScalar() {
		return scalar;
	}


	/**
	 * @param scalar
	 * @uml.property  name="scalar"
	 */
	public void setScalar(double scalar) {
		this.scalar = scalar;
	}


	/**
	 * @return
	 * @uml.property  name="scalar2"
	 */
	public String getScalar2() {
		return scalar2;
	}


	/**
	 * @param scalar2
	 * @uml.property  name="scalar2"
	 */
	public void setScalar2(String scalar2) {
		this.scalar2 = scalar2;
	}
	
	

}
