package project.permutations;

/**
 * @author  janis
 */
public class Symmetrizer {
	
	/**
	 * @uml.property  name="size"
	 */
	private int size;
	/**
	 * @uml.property  name="slotsIn"
	 */
	private int[] slotsIn;
	/**
	 * @uml.property  name="slotsOut"
	 */
	private int[] slotsOut;
	
	public Symmetrizer(int[] slotsIn, int[] slotsOut) {
		this.slotsIn = slotsIn;
		this.slotsOut = slotsOut;
		size = slotsIn.length;
	}

	/**
	 * @return
	 * @uml.property  name="size"
	 */
	public int getSize() {
		return size;
	}

	/**
	 * @param size
	 * @uml.property  name="size"
	 */
	public void setSize(int size) {
		this.size = size;
	}

	/**
	 * @return
	 * @uml.property  name="slotsIn"
	 */
	public int[] getSlotsIn() {
		return slotsIn;
	}

	/**
	 * @param slotsIn
	 * @uml.property  name="slotsIn"
	 */
	public void setSlotsIn(int[] slotsIn) {
		this.slotsIn = slotsIn;
	}

	/**
	 * @return
	 * @uml.property  name="slotsOut"
	 */
	public int[] getSlotsOut() {
		return slotsOut;
	}

	/**
	 * @param slotsOut
	 * @uml.property  name="slotsOut"
	 */
	public void setSlotsOut(int[] slotsOut) {
		this.slotsOut = slotsOut;
	}
	
	

}
