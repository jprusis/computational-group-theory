package project.permutations;

import java.util.ArrayList;
import java.util.Calendar;
import java.util.Collection;
import java.util.HashSet;
import java.util.LinkedList;
import java.util.List;
import java.util.Set;

public class Permutations<T> {
	
	private int[] permutation;
	private int[] testedValues;
	private int count;
    
	public Permutations() {
	
    }
	
	public LinkedList<List<Integer>> giveList(int start, int end) {
		Permutations<Integer> obj = new Permutations<Integer>();
        Collection<Integer> input = new ArrayList<Integer>();
        for (int i=start; i<end; i++) {
        	input.add(i);
        }

        Collection<List<Integer>> output = obj.permute(input);
        LinkedList<List<Integer>> pnr;
        int i=0;
        pnr = new LinkedList<List<Integer>>();
        for(List<Integer> integers : output){
        	//System.out.println(integers.subList(i, integers.size()));
            pnr.add(integers.subList(i, integers.size()));
        }
        return pnr;
	}
    
    
    public Collection<List<T>> permute(Collection<T> input) {
        Collection<List<T>> output = new ArrayList<List<T>>();
        if (input.isEmpty()) {
            output.add(new ArrayList<T>());
            return output;
        }
        List<T> list = new ArrayList<T>(input);
        T head = list.get(0);
        List<T> rest = list.subList(1, list.size());
        for (List<T> permutations : permute(rest)) {
            List<List<T>> subLists = new ArrayList<List<T>>();
            for (int i = 0; i <= permutations.size(); i++) {
                List<T> subList = new ArrayList<T>();
                subList.addAll(permutations);
                subList.add(i, head);
                subLists.add(subList);
            }
            output.addAll(subLists);
        }
        return output;
    }
    
    public boolean isOdd(List<Integer> currentList, int start) {
		int length = currentList.size();
		permutation = new int[length];
		testedValues = new int[length];
		for (int i=0; i<length; i++) {
			permutation[i] = currentList.get(i)-start;
		}
		count =0;
		for (int i=0; i<length; i++) {
			if (testedValues[i]==0) {
				returnParent(i, i);
			}
		}
		if ((count%2)==1) {return true;}
		else return false;
	}
	
	private int returnParent(int value, int valueVar) {
		if (value == permutation[valueVar]) {
			testedValues[valueVar]=1;
			return valueVar;
		} else {
			count++;
			testedValues[valueVar]=1;
			return returnParent(value, permutation[valueVar]);
		}
	}
}