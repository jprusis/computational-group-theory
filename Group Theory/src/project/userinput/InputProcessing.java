package project.userinput;
import java.util.ArrayList;
import java.util.LinkedList;

import project.composites.CompositeEdge;
import project.composites.CompositeGraph;
import project.composites.CompositeSlots;
import project.composites.CompositeVertex;
import project.composites.Tableau;
import project.graphs.Edge;
import project.graphs.Graph;
import project.graphs.GraphList;
import project.graphs.SlotList;
import project.graphs.Vertex;
import project.permutations.Antisymmetrizer;
import project.permutations.PermutationGraph;
import project.permutations.Symmetrizer;

public class InputProcessing {

	public Attributes createAttributes() {
		
		System.out.println("Processing input..");
		/////////////////////////////////////
		int group=0, method=0;
		
		int outputSample=3;
		/////////////////////////////////////
		double constant=1;
		String stringConst = "1";
		int dim=3;
		
		Attributes initialAttributes;
		LinkedList<Edge> edges = new LinkedList<Edge>();
		ArrayList<Edge> legs = new ArrayList<Edge>();
		LinkedList<Vertex> vertices = new LinkedList<Vertex>();
		LinkedList<Symmetrizer> sym = new LinkedList<Symmetrizer>(); 
		LinkedList<Antisymmetrizer> anti = new LinkedList<Antisymmetrizer>();
		LinkedList<CompositeEdge> comEdges = new LinkedList<CompositeEdge>();
		LinkedList<CompositeVertex> comVertices = new LinkedList<CompositeVertex>();
		
		if (outputSample==0) {
			edges.add(new Edge(1,2,0));
			edges.add(new Edge(3,4,0));
			edges.add(new Edge(5,6,0)); 
			vertices.add(new Vertex(1,3,5,2));
			vertices.add(new Vertex(2,4,6,2)); 
		}
		
		if (outputSample==1) {
			edges.add(new Edge(1,2,1));
			edges.add(new Edge(3,4,1));
			edges.add(new Edge(5,6,0)); 
			vertices.add(new Vertex(1,3,5,1));
			vertices.add(new Vertex(2,4,6,1)); 
		}
		
		if (outputSample==2) {
			edges.add(new Edge(1,2,1));
			edges.add(new Edge(3,4,1));
			edges.add(new Edge(5,6,1));
			edges.add(new Edge(7,8,0));
			edges.add(new Edge(9,10,0));
			edges.add(new Edge(11,12,0)); 
			vertices.add(new Vertex(1,7,6,1));
			vertices.add(new Vertex(2,3,9,1));
			vertices.add(new Vertex(4,5,11,1));
			vertices.add(new Vertex(8,10,12,2)); 
		}
	
		if (outputSample==3) {
			Edge leg1 = new Edge(2,14,2);
			edges.add(leg1);
			Edge leg2 = new Edge(5,15,2);
			edges.add(leg2);
			Edge leg3 = new Edge(7,16,2);
			edges.add(leg3);
			Edge leg4 = new Edge(10,13,2);
			edges.add(leg4);
			edges.add(new Edge(3,4,0));
			edges.add(new Edge(6,9,0));
			edges.add(new Edge(8,12,0));
			edges.add(new Edge(11,1,0));
			legs.add(leg1);
			legs.add(leg2);
			legs.add(leg3);
			legs.add(leg4);
			vertices.add(new Vertex(14,21,22,3)); 
			vertices.add(new Vertex(15,23,24,3)); 
			vertices.add(new Vertex(16,17,18,3)); 
			vertices.add(new Vertex(13,19,20,3)); 
			vertices.add(new Vertex(1,2,3,2)); 
			vertices.add(new Vertex(4,5,6,2)); 
			vertices.add(new Vertex(7,8,9,2)); 
			vertices.add(new Vertex(10,11,12,2)); 
		}
		
		if (outputSample==4) {
			Edge leg1 = new Edge(26,1,2);
			edges.add(leg1);
			Edge leg2 = new Edge(25,24,2);
			edges.add(leg2);
			edges.add(new Edge(3,4,0));
			edges.add(new Edge(2,7,0));
			edges.add(new Edge(8,10,0));
			edges.add(new Edge(9,19,0));
			edges.add(new Edge(5,13,0));
			edges.add(new Edge(6,11,0));
			edges.add(new Edge(12,18,0));
			edges.add(new Edge(16,15,0));
			edges.add(new Edge(17,20,0));
			edges.add(new Edge(14,23,0));
			edges.add(new Edge(21,22,0));
			vertices.add(new Vertex(1,3,2,2)); 
			vertices.add(new Vertex(4,5,6,2)); 
			vertices.add(new Vertex(7,8,9,2)); 
			vertices.add(new Vertex(10,11,12,2)); 
			vertices.add(new Vertex(13,14,15,2)); 
			vertices.add(new Vertex(16,17,18,2)); 
			vertices.add(new Vertex(19,20,21,2)); 
			vertices.add(new Vertex(22,23,24,2)); 
			vertices.add(new Vertex(25,27,28,3)); 
			vertices.add(new Vertex(26,29,30,3)); 
			legs.add(leg1);
			legs.add(leg2);
		}
		
		if (outputSample==5) {
			edges.add(new Edge(1,2,0));
			edges.add(new Edge(3,4,0));
			edges.add(new Edge(5,6,0));
			edges.add(new Edge(7,8,0));
			edges.add(new Edge(9,10,0));
			edges.add(new Edge(11,12,0)); 
			vertices.add(new Vertex(1,7,6,2));
			vertices.add(new Vertex(2,3,9,2));
			vertices.add(new Vertex(4,5,11,2));
			vertices.add(new Vertex(8,10,12,2)); 
		}
		
		if (outputSample==6) {
			edges.add(new Edge(1,2,1));
			edges.add(new Edge(3,4,1));
			edges.add(new Edge(5,6,1));
			int[] in = new int[3];
			in[0] = 2;
			in[1] = 4;
			in[2] = 6;
			int[] out = new int[3];
			out[0] = 1;
			out[1] = 3;
			out[2] = 5;
			anti.add(new Antisymmetrizer(in, out));
		}
		
		if (outputSample==7) {
			edges.add(new Edge(1,7,1));
			edges.add(new Edge(2,4,1));
			edges.add(new Edge(5,3,1));
			edges.add(new Edge(6,8,1));
			int[] inSym = {3,4};
			int[] outSym = {1,2};
			int[] inAnti = {7,8};
			int[] outAnti = {5,6};
			anti.add(new Antisymmetrizer(inAnti, outAnti));
			sym.add(new Symmetrizer(inSym, outSym));
			constant = (4.0/3.0);
			stringConst = "4/3";
		}
		
		if (outputSample==8) {
			
			int[] trivial = {1};
			int[] adjoint = {2,1};
			int[] back = {3,1};
			
			Tableau tab1 = new Tableau(trivial);
			Tableau tab2 = new Tableau(adjoint);
			Tableau tab3 = new Tableau(back);
			
			comEdges.add(new CompositeEdge(1,2,tab1));
			comEdges.add(new CompositeEdge(3,4,tab2));
			comEdges.add(new CompositeEdge(5,6,tab3));
			comEdges.add(new CompositeEdge(7,8,tab1));
			
			comVertices.add(new CompositeVertex(2,3,6));
			comVertices.add(new CompositeVertex(4,7,5));
			
		}
		
		if (outputSample==9) {
			edges.add(new Edge(3,5,1));
			edges.add(new Edge(4,10,1));
			edges.add(new Edge(7,9,1));
			edges.add(new Edge(8,14,1));
			edges.add(new Edge(11,13,1));
			edges.add(new Edge(12,20,1));
			edges.add(new Edge(16,19,1));
			edges.add(new Edge(17,24,1));
			edges.add(new Edge(18,2,1));
			edges.add(new Edge(21,23,1));
			edges.add(new Edge(22,15,1));
			edges.add(new Edge(25,27,1));
			edges.add(new Edge(26,28,1));
			edges.add(new Edge(29,1,1));
			edges.add(new Edge(30,6,1));
			int[] inSym1 = {1,2};
			int[] outSym1 = {3,4};
			int[] inSym2 = {9,10};
			int[] outSym2 = {11,12};
			int[] inSym3 = {19,20};
			int[] outSym3 = {21,22};
			
			int[] inAnti1 = {5,6};
			int[] outAnti1 = {7,8};
			int[] inAnti2 = {13,14,15};
			int[] outAnti2 = {16,17,18};
			int[] inAnti3 = {23,24};
			int[] outAnti3 = {25,26};
			int[] inAnti4 = {27,28};
			int[] outAnti4 = {29,30};
			anti.add(new Antisymmetrizer(inAnti1, outAnti1));
			anti.add(new Antisymmetrizer(inAnti2, outAnti2));
			anti.add(new Antisymmetrizer(inAnti3, outAnti3));
			anti.add(new Antisymmetrizer(inAnti4, outAnti4));
			sym.add(new Symmetrizer(inSym1, outSym1));
			sym.add(new Symmetrizer(inSym2, outSym2));
			sym.add(new Symmetrizer(inSym3, outSym3));
			constant = (8.0/3.0);
			stringConst = "8/3";
		}
		
if (outputSample==10) {
			
			int[] trivial = {1};
			int[] adjoint = {2,1};
			int[] back = {2,1,1};
			int[] reverse = {1,1};
			
			Tableau tab1 = new Tableau(trivial);
			Tableau tab2 = new Tableau(adjoint);
			Tableau tab3 = new Tableau(back);
			Tableau tab4 = new Tableau(reverse);
			
			comEdges.add(new CompositeEdge(1,2,tab1));
			comEdges.add(new CompositeEdge(3,5,tab3));
			comEdges.add(new CompositeEdge(10,4,tab2));
			comEdges.add(new CompositeEdge(7,8,tab1));
			comEdges.add(new CompositeEdge(6,11,tab2));
			comEdges.add(new CompositeEdge(13,9,tab4));
			comEdges.add(new CompositeEdge(12,14,tab1));
			
			comVertices.add(new CompositeVertex(2,3,4,1));
			comVertices.add(new CompositeVertex(5,6,7,1));
			comVertices.add(new CompositeVertex(8,9,10,1));
			comVertices.add(new CompositeVertex(11,12,13,1));
			
			dim=3;
		}
		
		/////////////////////////////////////////
		
		if (method == 0) {
			
			SlotList slots = new SlotList(edges, vertices);
			Graph initialGraph = new Graph(edges, vertices, slots, 1);
			GraphList initialGraphList = new GraphList(initialGraph);
		
			initialAttributes = new Attributes(initialGraphList, group, method);
			initialAttributes.setLegs(legs);
			
		} else if (method == 1) {
			
			SlotList slots = new SlotList(edges, vertices, sym, anti);
			PermutationGraph initialGraph = new PermutationGraph(edges, vertices, sym, anti, slots, constant);
			initialGraph.setScalar2(String.format(stringConst));
			GraphList initialGraphList = new GraphList(initialGraph);
			
			initialAttributes = new Attributes(initialGraphList, group, method);
			initialAttributes.setLegs(legs);
		
		} else if (method == 2) {
			
			CompositeSlots slots = new CompositeSlots(comEdges, comVertices);
			CompositeGraph initialGraph = new CompositeGraph(comEdges, comVertices, slots, dim);
			initialAttributes = new Attributes(initialGraph, group, method, dim);
			
		} else {
			
			initialAttributes = new Attributes();
		}
		
		return initialAttributes;
	}
	

}
