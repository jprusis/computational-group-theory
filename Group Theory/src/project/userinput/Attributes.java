package project.userinput;

import java.util.ArrayList;
import java.util.Hashtable;
import java.util.LinkedList;
import java.util.Set;

import project.composites.CompositeGraph;
import project.graphs.Edge;
import project.graphs.GraphList;

/**
 * @author  janis
 */
public class Attributes {
	

	/**
	 * @uml.property  name="graphs"
	 * @uml.associationEnd  
	 */
	private GraphList graphs;
	/**
	 * @uml.property  name="group"
	 */
	private int group;
	/**
	 * @uml.property  name="method"
	 */
	private int method;
	private int dim;
	



	/**
	 * @uml.property  name="legs"
	 */
	private ArrayList<Edge> legs;
	/**
	 * @uml.property  name="table"
	 */
	private Hashtable<Set<LinkedList<Edge>>, String> table;
	private CompositeGraph givenComp;

	
	public Attributes() {
		
	}
	
	
	
	public Attributes(GraphList initialGraphList, int group, int method) {
		graphs = initialGraphList;
		this.group = group;
		this.method = method;
		legs = null;
		table = new Hashtable<Set<LinkedList<Edge>>, String>();
	}
	
	public Attributes(CompositeGraph initialGraph, int group, int method, int dim) {
		givenComp = initialGraph;
		this.group = group;
		this.method = method;
		legs = null;
		table = new Hashtable<Set<LinkedList<Edge>>, String>();
		this.dim = dim;
	}

	/**
	 * @return
	 * @uml.property  name="table"
	 */
	public Hashtable<Set<LinkedList<Edge>>, String> getTable() {
		return table;
	}

	/**
	 * @param table
	 * @uml.property  name="table"
	 */
	public void setTable(
			Hashtable<Set<LinkedList<Edge>>, String> table) {
		this.table = table;
	}

	/**
	 * @return
	 * @uml.property  name="legs"
	 */
	public ArrayList<Edge> getLegs() {
		return legs;
	}

	/**
	 * @param legs
	 * @uml.property  name="legs"
	 */
	public void setLegs(ArrayList<Edge> legs) {
		this.legs = legs;
	}

	/**
	 * @return
	 * @uml.property  name="graphs"
	 */
	public GraphList getGraphs() {
		return graphs;
	}

	/**
	 * @return
	 * @uml.property  name="group"
	 */
	public int getGroup() {
		return group;
	}

	/**
	 * @return
	 * @uml.property  name="method"
	 */
	public int getMethod() {
		return method;
	}
	
	public CompositeGraph getGivenComp() {
		return givenComp;
	}



	public void setGivenComp(CompositeGraph givenComp) {
		this.givenComp = givenComp;
	}
	
	public int getDim() {
		return dim;
	}



	public void setDim(int dim) {
		this.dim = dim;
	}



	public void setGraphs(GraphList graphs) {
		this.graphs = graphs;
	}



	public void setGroup(int group) {
		this.group = group;
	}



	public void setMethod(int method) {
		this.method = method;
	}
	
}
