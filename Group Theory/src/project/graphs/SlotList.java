package project.graphs;

import java.util.Hashtable;
import java.util.LinkedList;

import project.permutations.Antisymmetrizer;
import project.permutations.Symmetrizer;

public class SlotList {
	
	private Hashtable<Integer, Vertex> verticeTable;
	private Hashtable<Integer, Edge> edgeTable;
	private Hashtable<Integer, Symmetrizer> symTable;
	private Hashtable<Integer, Antisymmetrizer> antiTable;

	public SlotList(LinkedList<Edge> givenEdges, LinkedList<Vertex> givenVertices) {
		Hashtable<Integer, Edge> edges = new Hashtable<Integer, Edge>();
		Hashtable<Integer, Vertex> vertices = new Hashtable<Integer, Vertex>();
		for (int i =0; i< givenEdges.size(); i++) {
			edges.put(givenEdges.get(i).getStart(), givenEdges.get(i));
			edges.put(givenEdges.get(i).getEnd(), givenEdges.get(i));
		}
		for (int i =0; i< givenVertices.size(); i++) {
			vertices.put(givenVertices.get(i).getSlot1(), givenVertices.get(i));
			vertices.put(givenVertices.get(i).getSlot2(), givenVertices.get(i));
			vertices.put(givenVertices.get(i).getSlot3(), givenVertices.get(i));
		}
		edgeTable = edges;
		verticeTable = vertices;
	}
	
	public SlotList(LinkedList<Symmetrizer> givenSym, LinkedList<Antisymmetrizer> givenAnti, int k) {
		
	}
	
	public SlotList(LinkedList<Edge> givenEdges, LinkedList<Vertex> givenVertices, LinkedList<Symmetrizer> givenSym, LinkedList<Antisymmetrizer> givenAnti) {
		Hashtable<Integer, Edge> edges = new Hashtable<Integer, Edge>();
		Hashtable<Integer, Vertex> vertices = new Hashtable<Integer, Vertex>();
		Hashtable<Integer, Symmetrizer> sym = new Hashtable<Integer, Symmetrizer>();
		Hashtable<Integer, Antisymmetrizer> anti = new Hashtable<Integer, Antisymmetrizer>();
		for (int i =0; i< givenEdges.size(); i++) {
			edges.put(givenEdges.get(i).getStart(), givenEdges.get(i));
			edges.put(givenEdges.get(i).getEnd(), givenEdges.get(i));
		}
		for (int i =0; i< givenVertices.size(); i++) {
			vertices.put(givenVertices.get(i).getSlot1(), givenVertices.get(i));
			vertices.put(givenVertices.get(i).getSlot2(), givenVertices.get(i));
			vertices.put(givenVertices.get(i).getSlot3(), givenVertices.get(i));
		}
		int[] in, out;
		Symmetrizer current;
		Antisymmetrizer current1;
		
		for (int i=0; i<givenSym.size(); i++) {
			current = givenSym.get(i);
			in = current.getSlotsIn();
			out = current.getSlotsOut();
			for (int j =0; j<in.length; j++) {
				sym.put(in[j], current);
				sym.put(out[j], current);
			}
		}
		for (int i=0; i<givenAnti.size(); i++) {
			current1 = givenAnti.get(i);
			in = current1.getSlotsIn();
			out = current1.getSlotsOut();
			for (int j =0; j<in.length; j++) {
				anti.put(in[j], current1);
				anti.put(out[j], current1);
			}
		}
		edgeTable = edges;
		verticeTable = vertices;
		symTable = sym;
		antiTable = anti;
	}

	public void putSym(Symmetrizer sym) {
		int[] in, out;
		in = sym.getSlotsIn();
		out = sym.getSlotsOut();
		for (int j =0; j<in.length; j++) {
			symTable.put(in[j], sym);
			symTable.put(out[j], sym);
		}
	}
	
	public void putAnti(Antisymmetrizer anti) {
		int[] in, out;
		in = anti.getSlotsIn();
		out = anti.getSlotsOut();
		for (int j =0; j<in.length; j++) {
			antiTable.put(in[j], anti);
			antiTable.put(out[j], anti);
		}
	}
	
	
	public Edge getEdge(int i) {
		return edgeTable.get(i);
	}
	
	public Vertex getVertex(int i) {
		return verticeTable.get(i);
	}
	
	public int getNumElements() {
		int slotsVertices = verticeTable.size();
		int slotsEdges = edgeTable.size();
		if (slotsVertices>slotsEdges) {return slotsVertices;}
		else return slotsEdges;
	}
	
	public void putVertex(int slot, Vertex givenVertex) {
		verticeTable.put(slot, givenVertex);
	}
	
	public void putEdge(int slot, Edge givenEdge) {
		edgeTable.put(slot, givenEdge);
	}

}
