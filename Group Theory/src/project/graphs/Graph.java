package project.graphs;

import java.util.LinkedList;
import java.util.ListIterator;

import javax.script.ScriptEngine;
import javax.script.ScriptEngineManager;
import javax.script.ScriptException;

/**
 * @author  janis
 */
public class Graph {
	
	/**
	 * @uml.property  name="edges"
	 */
	private LinkedList<Edge> edges;
	/**
	 * @uml.property  name="vertices"
	 */
	private LinkedList<Vertex> vertices;
	/**
	 * @uml.property  name="slots"
	 * @uml.associationEnd  
	 */
	private SlotList slots;
	/**
	 * @uml.property  name="scalar"
	 */
	private double scalar;
	/**
	 * @uml.property  name="scalar2"
	 */
	private String scalar2;

	public Graph(LinkedList<Edge> edges, LinkedList<Vertex> vertices, SlotList slots, double i) {
		this.edges = edges;
		this.vertices = vertices;
		this.slots = slots;
		scalar = i;
		scalar2 = "1";
	}

	/**
	 * @return
	 * @uml.property  name="scalar2"
	 */
	public String getScalar2() {
		return scalar2;
	}

	/**
	 * @param scalar2
	 * @uml.property  name="scalar2"
	 */
	public void setScalar2(String scalar2) {
		this.scalar2 = scalar2;
	}

	/**
	 * @return
	 * @uml.property  name="edges"
	 */
	public LinkedList<Edge> getEdges() {
		return edges;
	}

	/**
	 * @param edges
	 * @uml.property  name="edges"
	 */
	public void setEdges(LinkedList<Edge> edges) {
		this.edges = edges;
	}

	/**
	 * @return
	 * @uml.property  name="vertices"
	 */
	public LinkedList<Vertex> getVertices() {
		return vertices;
	}

	/**
	 * @param vertices
	 * @uml.property  name="vertices"
	 */
	public void setVertices(LinkedList<Vertex> vertices) {
		this.vertices = vertices;
	}

	/**
	 * @return
	 * @uml.property  name="slots"
	 */
	public SlotList getSlots() {
		return slots;
	}

	/**
	 * @param slots
	 * @uml.property  name="slots"
	 */
	public void setSlots(SlotList slots) {
		this.slots = slots;
	}

	/**
	 * @return
	 * @uml.property  name="scalar"
	 */
	public double getScalar() {
		return scalar;
	}

	/**
	 * @param scalar
	 * @uml.property  name="scalar"
	 */
	public void setScalar(double scalar) {
		this.scalar = scalar;
	}

	public Graph insertPositiveGGG(int i) {
		Vertex gggVertex = vertices.get(i);
		int slot1 = gggVertex.getSlot1();
		int slot2 = gggVertex.getSlot2();
		int slot3 = gggVertex.getSlot3();
		int numSlots = slots.getNumElements();
		
		LinkedList<Edge> graph1Edges = new LinkedList<Edge>();
		for (int k=0; k<edges.size();k++) {
			graph1Edges.add(edges.get(k));
		}
		LinkedList<Vertex> graph1Vertices = new LinkedList<Vertex>();
		for (int k=0; k<vertices.size();k++) {
			graph1Vertices.add(vertices.get(k));
		}
		SlotList slotsGraph1 = new SlotList(graph1Edges, graph1Vertices);
		graph1Vertices.removeFirstOccurrence(gggVertex);
		
		Vertex newVertex1 = new Vertex(slot1, numSlots + 1, numSlots +2, 1);
		graph1Vertices.add(newVertex1);
		slotsGraph1.putVertex(slot1, newVertex1);
		slotsGraph1.putVertex(numSlots + 1, newVertex1);
		slotsGraph1.putVertex(numSlots + 2, newVertex1);
		
		Vertex newVertex2 = new Vertex(slot2, numSlots + 3, numSlots +4, 1);
		graph1Vertices.add(newVertex2);
		slotsGraph1.putVertex(slot2, newVertex2);
		slotsGraph1.putVertex(numSlots + 3, newVertex1);
		slotsGraph1.putVertex(numSlots + 4, newVertex1);
		
		Vertex newVertex3 = new Vertex(numSlots+5, numSlots + 6, slot3, 1);
		graph1Vertices.add(newVertex3);
		slotsGraph1.putVertex(slot3, newVertex3);
		slotsGraph1.putVertex(numSlots + 5, newVertex3);
		slotsGraph1.putVertex(numSlots + 6, newVertex3);
		
		Edge edge1Graph1 = new Edge(numSlots +6, numSlots +3, 1);
		graph1Edges.add(edge1Graph1);
		slotsGraph1.putEdge(numSlots + 6, edge1Graph1);
		slotsGraph1.putEdge(numSlots + 3, edge1Graph1);
		Edge edge2Graph1 = new Edge(numSlots +4, numSlots +1, 1);
		graph1Edges.add(edge2Graph1);
		slotsGraph1.putEdge(numSlots + 4, edge2Graph1);
		slotsGraph1.putEdge(numSlots + 1, edge2Graph1);
		Edge edge3Graph1 = new Edge(numSlots +2, numSlots +5, 1);
		graph1Edges.add(edge3Graph1);
		slotsGraph1.putEdge(numSlots + 2, edge3Graph1);
		slotsGraph1.putEdge(numSlots + 5, edge3Graph1);
		
		Graph graph1 = new Graph(graph1Edges, graph1Vertices, slotsGraph1, scalar);
		graph1.setScalar2(scalar2);
		return graph1;
	}
	
	public Graph insertNegativeGGG(int i) {
		
		Vertex gggVertex = vertices.get(i);
		int slot1 = gggVertex.getSlot1();
		int slot2 = gggVertex.getSlot2();
		int slot3 = gggVertex.getSlot3();
		int numSlots = slots.getNumElements();
		
		LinkedList<Edge> graph1Edges = new LinkedList<Edge>();
		for (int k=0; k<edges.size();k++) {
			graph1Edges.add(edges.get(k));
		}
		LinkedList<Vertex> graph1Vertices = new LinkedList<Vertex>();
		for (int k=0; k<vertices.size();k++) {
			graph1Vertices.add(vertices.get(k));
		}
		SlotList slotsGraph1 = new SlotList(graph1Edges, graph1Vertices);
		graph1Vertices.removeFirstOccurrence(gggVertex);
		
		Vertex newVertex1 = new Vertex(slot1, numSlots + 1, numSlots +2, 1);
		graph1Vertices.add(newVertex1);
		slotsGraph1.putVertex(slot1, newVertex1);
		slotsGraph1.putVertex(numSlots + 1, newVertex1);
		slotsGraph1.putVertex(numSlots + 2, newVertex1);
		
		Vertex newVertex2 = new Vertex(slot2, numSlots + 3, numSlots +4, 1);
		graph1Vertices.add(newVertex2);
		slotsGraph1.putVertex(slot2, newVertex2);
		slotsGraph1.putVertex(numSlots + 3, newVertex1);
		slotsGraph1.putVertex(numSlots + 4, newVertex1);
		
		Vertex newVertex3 = new Vertex(numSlots+5, numSlots + 6, slot3, 1);
		graph1Vertices.add(newVertex3);
		slotsGraph1.putVertex(slot3, newVertex3);
		slotsGraph1.putVertex(numSlots + 5, newVertex3);
		slotsGraph1.putVertex(numSlots + 6, newVertex3);
		
		Edge edge1Graph1 = new Edge(numSlots +3, numSlots +6, 1);
		graph1Edges.add(edge1Graph1);
		slotsGraph1.putEdge(numSlots + 6, edge1Graph1);
		slotsGraph1.putEdge(numSlots + 3, edge1Graph1);
		Edge edge2Graph1 = new Edge(numSlots +1, numSlots +4, 1);
		graph1Edges.add(edge2Graph1);
		slotsGraph1.putEdge(numSlots + 4, edge2Graph1);
		slotsGraph1.putEdge(numSlots + 1, edge2Graph1);
		Edge edge3Graph1 = new Edge(numSlots +5, numSlots +2, 1);
		graph1Edges.add(edge3Graph1);
		slotsGraph1.putEdge(numSlots + 2, edge3Graph1);
		slotsGraph1.putEdge(numSlots + 5, edge3Graph1);
		
		ScriptEngineManager scriptManager = new ScriptEngineManager();
	    ScriptEngine engine_1 = scriptManager.getEngineByExtension("m");
	    
		String input = "-(" + scalar2 + ")";
		String output = "";
		try {
			output = (String) engine_1.eval(input);
		} catch (ScriptException e) {
			e.printStackTrace();
		}
		
		Graph graph1 = new Graph(graph1Edges, graph1Vertices, slotsGraph1, (-1)*scalar);
		graph1.setScalar2(output);
		return graph1;
		
	}

	public Vertex findVertex(int i) {
		
		int firstSlot = getGSlot(vertices.get(i));
		Edge gEdge = slots.getEdge(firstSlot);
		int secondSlot;
		if (gEdge.getStart()==firstSlot) {secondSlot=gEdge.getEnd();}
		else {secondSlot=gEdge.getStart();}
		if (slots.getVertex(secondSlot).getType()==1) {return slots.getVertex(secondSlot);}
		else return null;
	}

	public Graph reduceQGQ_SUNReturnGraph1(int i) {
		Vertex firstVertex = vertices.get(i);
		int firstGSlot = getGSlot(firstVertex);
		Edge gEdge = slots.getEdge(firstGSlot);
		int otherGSlot;
		if (gEdge.getStart()==firstGSlot) {otherGSlot=gEdge.getEnd();}
		else {otherGSlot=gEdge.getStart();}
		Vertex otherVertex = slots.getVertex(otherGSlot);	 
		
		LinkedList<Edge> graph1Edges = new LinkedList<Edge>();
		for (int k=0; k<edges.size();k++) {
			graph1Edges.add(edges.get(k));
		}
		LinkedList<Vertex> graph1Vertices = new LinkedList<Vertex>();
		for (int k=0; k<vertices.size();k++) {
			graph1Vertices.add(vertices.get(k));
		}
		graph1Edges.removeFirstOccurrence(gEdge);
		SlotList slotsGraph1 = new SlotList(graph1Edges, graph1Vertices);
		graph1Vertices.removeFirstOccurrence(firstVertex);
		graph1Vertices.removeFirstOccurrence(otherVertex);
		
		Vertex newVertex1 = new Vertex(firstVertex.getSlot1(), firstVertex.getSlot2(), firstVertex.getSlot3(), 0);
		slotsGraph1.putVertex(firstVertex.getSlot1(), newVertex1);
		slotsGraph1.putVertex(firstVertex.getSlot2(), newVertex1);
		slotsGraph1.putVertex(firstVertex.getSlot3(), newVertex1);
		graph1Vertices.add(newVertex1);
		Vertex newVertex2 = new Vertex(otherVertex.getSlot1(), otherVertex.getSlot2(), otherVertex.getSlot3(), 0);
		graph1Vertices.add(newVertex2);
		slotsGraph1.putVertex(otherVertex.getSlot1(), newVertex2);
		slotsGraph1.putVertex(otherVertex.getSlot2(), newVertex2);
		slotsGraph1.putVertex(otherVertex.getSlot3(), newVertex2);
		
		ScriptEngineManager scriptManager = new ScriptEngineManager();
	    ScriptEngine engine_1 = scriptManager.getEngineByExtension("m");
	    
		String input = "-(" + scalar2 + ")/n";
		String output = "";
		try {
			output = (String) engine_1.eval(input);
		} catch (ScriptException e) {
			e.printStackTrace();
		}
		
		Graph graph1 = new Graph(graph1Edges, graph1Vertices, slotsGraph1, (-0.1)*scalar);
		graph1.setScalar2(output);
		
		return graph1;
	}
		
	public Graph reduceQGQ_SUNReturnGraph2(int i) {
		
		Vertex firstVertex = vertices.get(i);
		int firstGSlot = getGSlot(firstVertex);
		Edge gEdge = slots.getEdge(firstGSlot);
		int otherGSlot;
		if (gEdge.getStart()==firstGSlot) {otherGSlot=gEdge.getEnd();}
		else {otherGSlot=gEdge.getStart();}
		Vertex otherVertex = slots.getVertex(otherGSlot);
		
		LinkedList<Edge> graph2Edges = new LinkedList<Edge>();
		for (int k=0; k<edges.size();k++) {
			graph2Edges.add(edges.get(k));
		}
		LinkedList<Vertex> graph2Vertices = new LinkedList<Vertex>();
		for (int k=0; k<vertices.size();k++) {
			graph2Vertices.add(vertices.get(k));
		}
		graph2Edges.removeFirstOccurrence(gEdge);
		SlotList slotsGraph2 = new SlotList(graph2Edges, graph2Vertices);
		graph2Vertices.removeFirstOccurrence(firstVertex);
		graph2Vertices.removeFirstOccurrence(otherVertex);
		
		int numSlots = slots.getNumElements();
		
		int count = 0;
		int[] v1Slots = new int[2];
		for (int vSlot=1;vSlot<4;vSlot++) {
			if (firstVertex.getSlot(vSlot) != firstGSlot) {
				v1Slots[count] = firstVertex.getSlot(vSlot);
				count++;
			}
		}
		
		count = 0;
		int[] v2Slots = new int[2];
		for (int vSlot=1;vSlot<4;vSlot++) {
			if (otherVertex.getSlot(vSlot) != otherGSlot) {
				v2Slots[count] = otherVertex.getSlot(vSlot);
				count++;
			}
		}
		
		Vertex newVertex1 = new Vertex(v1Slots[0],numSlots+1, numSlots+3);
		slotsGraph2.putVertex(v1Slots[0], newVertex1);
		slotsGraph2.putVertex(numSlots+1, newVertex1);
		slotsGraph2.putVertex(numSlots+3, newVertex1);
		Vertex newVertex2 = new Vertex(v2Slots[0],numSlots+4, numSlots+2);
		slotsGraph2.putVertex(v2Slots[0], newVertex2);
		slotsGraph2.putVertex(numSlots+2, newVertex2);
		slotsGraph2.putVertex(numSlots+4, newVertex2);
		Vertex newVertex3 = new Vertex(v1Slots[1],numSlots+5, firstGSlot);
		slotsGraph2.putVertex(v1Slots[1], newVertex3);
		slotsGraph2.putVertex(numSlots+5, newVertex3);
		slotsGraph2.putVertex(firstGSlot, newVertex3);
		Vertex newVertex4 = new Vertex(v2Slots[1],otherGSlot, numSlots+6);
		slotsGraph2.putVertex(v2Slots[1], newVertex4);
		slotsGraph2.putVertex(numSlots+6, newVertex4);
		slotsGraph2.putVertex(otherGSlot, newVertex4);
		graph2Vertices.add(newVertex1);
		graph2Vertices.add(newVertex2);
		graph2Vertices.add(newVertex3);
		graph2Vertices.add(newVertex4);
		
		Edge newEdge, newEdge2;
		if (slots.getEdge(v1Slots[0]).getEnd() == v1Slots[0]) {
			if (slots.getEdge(v2Slots[0]).getStart() == v2Slots[0]) {
				newEdge = new Edge(numSlots +1, numSlots +2, 1);
				newEdge2 = new Edge(otherGSlot, firstGSlot, 1);
			}
			else {
				newEdge = new Edge(numSlots +1, otherGSlot, 1);
				newEdge2 = new Edge(numSlots +2, firstGSlot, 1);
			}
		}
		else {
			if (slots.getEdge(v2Slots[0]).getEnd() == v2Slots[0]) {
				newEdge = new Edge(numSlots +2, numSlots +1, 1);
				newEdge2 = new Edge(firstGSlot, otherGSlot, 1);
			}
			else {
				newEdge = new Edge(otherGSlot, numSlots +1, 1);
				newEdge2 = new Edge(firstGSlot, numSlots+2, 1);
			}
		}
		
		slotsGraph2.putEdge(newEdge.getStart(), newEdge);
		slotsGraph2.putEdge(newEdge.getEnd(), newEdge);
		slotsGraph2.putEdge(newEdge2.getStart(), newEdge2);
		slotsGraph2.putEdge(newEdge2.getEnd(), newEdge2);
		graph2Edges.add(newEdge);
		graph2Edges.add(newEdge2);
		Graph graph2 = new Graph(graph2Edges, graph2Vertices, slotsGraph2, scalar);
		graph2.setScalar2(scalar2);
		return graph2;
	}
	
	private int getGSlot(Vertex givenVertex) {
		int firstSlot = givenVertex.getSlot1();
		Edge gEdge = slots.getEdge(firstSlot);
		if (gEdge.getType() != 0) {
			firstSlot = givenVertex.getSlot2();
			gEdge = slots.getEdge(firstSlot);
			}
		if (gEdge.getType() != 0) {
			firstSlot = givenVertex.getSlot3();
			}
		return firstSlot;
	}
	
	public int enumerateVertices() {
		int num=0;
		ListIterator<Vertex> iterator = vertices.listIterator();
		while (iterator.hasNext()) {
			Vertex current = iterator.next();
			current.setNum(num);
			num++;
		}
		return num;
		
	}

	public Graph reduceQGQ_SONReturnGraph1(int i) {

		Vertex firstVertex = vertices.get(i);
		int firstGSlot = getGSlot(firstVertex);
		Edge gEdge = slots.getEdge(firstGSlot);
		int otherGSlot;
		if (gEdge.getStart()==firstGSlot) {otherGSlot=gEdge.getEnd();}
		else {otherGSlot=gEdge.getStart();}
		Vertex otherVertex = slots.getVertex(otherGSlot);
		
		LinkedList<Edge> graph2Edges = new LinkedList<Edge>();
		for (int k=0; k<edges.size();k++) {
			graph2Edges.add(edges.get(k));
		}
		LinkedList<Vertex> graph2Vertices = new LinkedList<Vertex>();
		for (int k=0; k<vertices.size();k++) {
			graph2Vertices.add(vertices.get(k));
		}
		graph2Edges.removeFirstOccurrence(gEdge);
		SlotList slotsGraph2 = new SlotList(graph2Edges, graph2Vertices);
		graph2Vertices.removeFirstOccurrence(firstVertex);
		graph2Vertices.removeFirstOccurrence(otherVertex);
		
		int numSlots = slots.getNumElements();
		
		int count = 0;
		int[] v1Slots = new int[2];
		for (int vSlot=1;vSlot<4;vSlot++) {
			if (firstVertex.getSlot(vSlot) != firstGSlot) {
				v1Slots[count] = firstVertex.getSlot(vSlot);
				count++;
			}
		}
		
		count = 0;
		int[] v2Slots = new int[2];
		for (int vSlot=1;vSlot<4;vSlot++) {
			if (otherVertex.getSlot(vSlot) != otherGSlot) {
				v2Slots[count] = otherVertex.getSlot(vSlot);
				count++;
			}
		}
		
		Vertex newVertex1 = new Vertex(v1Slots[0],numSlots+1, numSlots+3);
		slotsGraph2.putVertex(v1Slots[0], newVertex1);
		slotsGraph2.putVertex(numSlots+1, newVertex1);
		slotsGraph2.putVertex(numSlots+3, newVertex1);
		Vertex newVertex2 = new Vertex(v2Slots[0],numSlots+4, numSlots+2);
		slotsGraph2.putVertex(v2Slots[0], newVertex2);
		slotsGraph2.putVertex(numSlots+2, newVertex2);
		slotsGraph2.putVertex(numSlots+4, newVertex2);
		Vertex newVertex3 = new Vertex(v1Slots[1],numSlots+5, firstGSlot);
		slotsGraph2.putVertex(v1Slots[1], newVertex3);
		slotsGraph2.putVertex(numSlots+5, newVertex3);
		slotsGraph2.putVertex(firstGSlot, newVertex3);
		Vertex newVertex4 = new Vertex(v2Slots[1],otherGSlot, numSlots+6);
		slotsGraph2.putVertex(v2Slots[1], newVertex4);
		slotsGraph2.putVertex(numSlots+6, newVertex4);
		slotsGraph2.putVertex(otherGSlot, newVertex4);
		graph2Vertices.add(newVertex1);
		graph2Vertices.add(newVertex2);
		graph2Vertices.add(newVertex3);
		graph2Vertices.add(newVertex4);
		
		Edge newEdge, newEdge2;
		if (slots.getEdge(v1Slots[0]).getEnd() == v1Slots[0]) {
			if (slots.getEdge(v2Slots[0]).getStart() == v2Slots[0]) {
				newEdge = new Edge(numSlots +1, numSlots +2, 1);
				newEdge2 = new Edge(otherGSlot, firstGSlot, 1);
			}
			else {
				newEdge = new Edge(numSlots +1, otherGSlot, 1);
				newEdge2 = new Edge(numSlots +2, firstGSlot, 1);
			}
		}
		else {
			if (slots.getEdge(v2Slots[0]).getEnd() == v2Slots[0]) {
				newEdge = new Edge(numSlots +2, numSlots +1, 1);
				newEdge2 = new Edge(firstGSlot, otherGSlot, 1);
			}
			else {
				newEdge = new Edge(otherGSlot, numSlots +1, 1);
				newEdge2 = new Edge(firstGSlot, numSlots+2, 1);
			}
		}
		
		ScriptEngineManager scriptManager = new ScriptEngineManager();
	    ScriptEngine engine_1 = scriptManager.getEngineByExtension("m");
	    
		String input = "(" + scalar2 + ")/2";
		String output = "";
		try {
			output = (String) engine_1.eval(input);
		} catch (ScriptException e) {
			e.printStackTrace();
		}
		
		slotsGraph2.putEdge(newEdge.getStart(), newEdge);
		slotsGraph2.putEdge(newEdge.getEnd(), newEdge);
		slotsGraph2.putEdge(newEdge2.getStart(), newEdge2);
		slotsGraph2.putEdge(newEdge2.getEnd(), newEdge2);
		graph2Edges.add(newEdge);
		graph2Edges.add(newEdge2);
		Graph graph2 = new Graph(graph2Edges, graph2Vertices, slotsGraph2, scalar*0.5);
		graph2.setScalar2(output);
		return graph2;
	}

	public Graph reduceQGQ_SONReturnGraph2(int i) {

		Vertex firstVertex = vertices.get(i);
		int firstGSlot = getGSlot(firstVertex);
		Edge gEdge = slots.getEdge(firstGSlot);
		int otherGSlot;
		if (gEdge.getStart()==firstGSlot) {otherGSlot=gEdge.getEnd();}
		else {otherGSlot=gEdge.getStart();}
		Vertex otherVertex = slots.getVertex(otherGSlot);
		
		LinkedList<Edge> graph2Edges = new LinkedList<Edge>();
		for (int k=0; k<edges.size();k++) {
			graph2Edges.add(edges.get(k));
		}
		LinkedList<Vertex> graph2Vertices = new LinkedList<Vertex>();
		for (int k=0; k<vertices.size();k++) {
			graph2Vertices.add(vertices.get(k));
		}
		graph2Edges.removeFirstOccurrence(gEdge);
		SlotList slotsGraph2 = new SlotList(graph2Edges, graph2Vertices);
		graph2Vertices.removeFirstOccurrence(firstVertex);
		graph2Vertices.removeFirstOccurrence(otherVertex);
		
		int numSlots = slots.getNumElements();
		
		int count = 0;
		int[] v1Slots = new int[2];
		for (int vSlot=1;vSlot<4;vSlot++) {
			if (firstVertex.getSlot(vSlot) != firstGSlot) {
				v1Slots[count] = firstVertex.getSlot(vSlot);
				count++;
			}
		}
		
		count = 0;
		int[] v2Slots = new int[2];
		for (int vSlot=1;vSlot<4;vSlot++) {
			if (otherVertex.getSlot(vSlot) != otherGSlot) {
				v2Slots[count] = otherVertex.getSlot(vSlot);
				count++;
			}
		}
		
		Vertex newVertex1 = new Vertex(v1Slots[0],numSlots+1, numSlots+3);
		slotsGraph2.putVertex(v1Slots[0], newVertex1);
		slotsGraph2.putVertex(numSlots+1, newVertex1);
		slotsGraph2.putVertex(numSlots+3, newVertex1);
		Vertex newVertex2 = new Vertex(v2Slots[0],numSlots+4, numSlots+2);
		slotsGraph2.putVertex(v2Slots[0], newVertex2);
		slotsGraph2.putVertex(numSlots+2, newVertex2);
		slotsGraph2.putVertex(numSlots+4, newVertex2);
		Vertex newVertex3 = new Vertex(v1Slots[1],numSlots+5, firstGSlot);
		slotsGraph2.putVertex(v1Slots[1], newVertex3);
		slotsGraph2.putVertex(numSlots+5, newVertex3);
		slotsGraph2.putVertex(firstGSlot, newVertex3);
		Vertex newVertex4 = new Vertex(v2Slots[1],otherGSlot, numSlots+6);
		slotsGraph2.putVertex(v2Slots[1], newVertex4);
		slotsGraph2.putVertex(numSlots+6, newVertex4);
		slotsGraph2.putVertex(otherGSlot, newVertex4);
		graph2Vertices.add(newVertex1);
		graph2Vertices.add(newVertex2);
		graph2Vertices.add(newVertex3);
		graph2Vertices.add(newVertex4);
		
		Edge newEdge, newEdge2;
		if (slots.getEdge(v1Slots[0]).getEnd() == v1Slots[0]) {
			if (slots.getEdge(v2Slots[0]).getStart() == v2Slots[0]) {
				newEdge = new Edge(numSlots +1, firstGSlot, 1);
				newEdge2 = new Edge(otherGSlot, numSlots +2, 1);
			}
			else {
				newEdge = new Edge(numSlots +1,firstGSlot , 1);
				newEdge2 = new Edge(numSlots +2, otherGSlot, 1);
			}
		}
		else {
			if (slots.getEdge(v2Slots[0]).getEnd() == v2Slots[0]) {
				newEdge = new Edge(numSlots +2, otherGSlot, 1);
				newEdge2 = new Edge(firstGSlot, numSlots +1, 1);
			}
			else {
				newEdge = new Edge(otherGSlot, numSlots+2, 1);
				newEdge2 = new Edge(firstGSlot, numSlots +1, 1);
			}
		}
		
		ScriptEngineManager scriptManager = new ScriptEngineManager();
	    ScriptEngine engine_1 = scriptManager.getEngineByExtension("m");
	    
		String input = "-(" + scalar2 + ")/2";
		String output = "";
		try {
			output = (String) engine_1.eval(input);
		} catch (ScriptException e) {
			e.printStackTrace();
		}
		
		slotsGraph2.putEdge(newEdge.getStart(), newEdge);
		slotsGraph2.putEdge(newEdge.getEnd(), newEdge);
		slotsGraph2.putEdge(newEdge2.getStart(), newEdge2);
		slotsGraph2.putEdge(newEdge2.getEnd(), newEdge2);
		graph2Edges.add(newEdge);
		graph2Edges.add(newEdge2);
		Graph graph2 = new Graph(graph2Edges, graph2Vertices, slotsGraph2, (-0.5)*scalar);
		graph2.setScalar2(output);
		return graph2;
	}

}
