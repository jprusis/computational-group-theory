package project.graphs;

/**
 * @author  janis
 */
public class Vertex {
	
	/**
	 * @uml.property  name="slot1"
	 */
	private int slot1;
	/**
	 * @uml.property  name="slot2"
	 */
	private int slot2;
	/**
	 * @uml.property  name="slot3"
	 */
	private int slot3;
	/**
	 * @uml.property  name="type"
	 */
	private int type;
	/**
	 * @uml.property  name="num"
	 */
	private int num;

	public Vertex(int slot1, int slot2, int slot3, int type) {
		this.slot1 = slot1;
		this.slot2 = slot2;
		this.slot3 = slot3;
		this.type = type;
		num=0;
	}
	
	public Vertex(int slot1, int slot2, int slot3) {
		this.slot1 = slot1;
		this.slot2 = slot2;
		this.slot3 = slot3;
		type = 0;
		num=0;
		
	}
	
	/**
	 * @return
	 * @uml.property  name="slot1"
	 */
	public int getSlot1() {
		return slot1;
	}

	/**
	 * @param slot1
	 * @uml.property  name="slot1"
	 */
	public void setSlot1(int slot1) {
		this.slot1 = slot1;
	}

	/**
	 * @return
	 * @uml.property  name="slot2"
	 */
	public int getSlot2() {
		return slot2;
	}

	/**
	 * @param slot2
	 * @uml.property  name="slot2"
	 */
	public void setSlot2(int slot2) {
		this.slot2 = slot2;
	}

	/**
	 * @return
	 * @uml.property  name="type"
	 */
	public int getType() {
		return type;
	}

	/**
	 * @param type
	 * @uml.property  name="type"
	 */
	public void setType(int type) {
		this.type = type;
	}

	/**
	 * @return
	 * @uml.property  name="slot3"
	 */
	public int getSlot3() {
		return slot3;
	}
	
	public int getSlot(int i) {
		if (i==1) {return slot1;}
		else if (i==2) {return slot2;}
		else {return slot3;}
	}

	/**
	 * @param slot3
	 * @uml.property  name="slot3"
	 */
	public void setSlot3(int slot3) {
		this.slot3 = slot3;
	}

	public void setSlot(int vSlot, int i) {
		if (vSlot==1) {slot1=i;}
		else if (vSlot==2) {slot2=i;}
		else {slot3=i;}
		
	}

	/**
	 * @return
	 * @uml.property  name="num"
	 */
	public int getNum() {
		return num;
	}

	/**
	 * @param num
	 * @uml.property  name="num"
	 */
	public void setNum(int num) {
		this.num = num;
	}
	
}
