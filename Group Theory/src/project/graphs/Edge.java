package project.graphs;

/**
 * @author  janis
 */
public class Edge {

/**
 * @uml.property  name="start"
 */
protected int start;
/**
 * @uml.property  name="end"
 */
protected int end;
/**
 * @uml.property  name="type"
 */
protected int type;
	
	// initiation method stores provided parameters within instance
	
	public Edge(int start, int end, int type) {
		this.start = start;
		this.end = end;
		this.type = type;
	}
	
	public Edge(Edge givenEdge) {
		start = givenEdge.getStart();
		end = givenEdge.getEnd();
		type = givenEdge.getType();
	}
	
	// getStart returns start of the edge
	
	/**
	 * @return
	 * @uml.property  name="start"
	 */
	public int getStart() {
		return start;
	}
	
	// getStart returns end of the edge
	
	/**
	 * @return
	 * @uml.property  name="end"
	 */
	public int getEnd() {
		return end;
	}
	
	// getType returns type of the edge
	
	/**
	 * @return
	 * @uml.property  name="type"
	 */
	public int getType() {
		return type;
	}
	
	// setStart sets new start of the edge
	
	/**
	 * @param start
	 * @uml.property  name="start"
	 */
	public void setStart(int start) {
		this.start = start;
	}
	
	// setEnd sets new end of the edge
	
	/**
	 * @param end
	 * @uml.property  name="end"
	 */
	public void setEnd(int end) {
		this.end = end;
	}
	
	/**
	 * @param type
	 * @uml.property  name="type"
	 */
	public void setType(int type) {
		this.type = type;
	}
	
}
