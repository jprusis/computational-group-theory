package project.graphs;

import java.util.LinkedList;
import java.util.ListIterator;

import project.permutations.PermutationGraph;

public class GraphList {
	
	private int method;
	private LinkedList<Graph> currentList;
	private ListIterator<Graph> iterator;
	private LinkedList<PermutationGraph> currentList1;
	private ListIterator<PermutationGraph> iterator1;
	
	public GraphList(Graph initialGraph) {
		currentList = new LinkedList<Graph>();
		currentList.add(initialGraph);
		method =0;
	}
	
	public GraphList(PermutationGraph initialGraph) {
		currentList1 = new LinkedList<PermutationGraph>();
		currentList1.add(initialGraph);
		method =1;
	}
	
	public void createIterator() {
		if (method==0) { iterator =  currentList.listIterator(); }
		if (method==1) { iterator1 =  currentList1.listIterator(); }
	}
	
	public Graph getNextGraph() {
		
		return iterator.next();
		
	}
	
	public PermutationGraph getNextPermutationGraph() {
		return iterator1.next();
	}
	
	public boolean checkNextGraph() {
		if (method==0) { return iterator.hasNext(); }
		else { return iterator1.hasNext(); }
	}
	
	public void addGraph(Graph givenGraph) {
		currentList.add(givenGraph);
	}
	
	public void addToIterator(Graph givenGraph) {
		iterator.add(givenGraph);
	}
	
	public void addGraph(PermutationGraph givenGraph) {
		currentList1.add(givenGraph);
	}
	
	public void addToIterator(PermutationGraph givenGraph) {
		iterator1.add(givenGraph);
	}

	public void removeCurrent() {
		if (method==0) { iterator.remove(); }
		if (method==1) { iterator1.remove(); }
	}
	
	public int getSize() {
		if (method==0) { return currentList.size(); }
		else { return currentList1.size(); }
	}
	
	public Graph getElement(int i) {
		return currentList.get(i);
	}
	
	public PermutationGraph getPermutationElement(int i) {
		return currentList1.get(i);
	}

}
