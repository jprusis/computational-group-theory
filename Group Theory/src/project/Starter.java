package project;

import project.controller.Controller;

public class Starter {
	
	public static void main(String[] args) {
			
		new Controller().processRequest();
	}

}
