package project.controller;

import project.findingloops.Loops;
import project.graphs.Graph;
import project.graphs.GraphList;
import project.output.GenerateOutput;
import project.permutations.PermutationGraph;
import project.reduction.GraphReducer;
import project.userinput.Attributes;
import project.userinput.InputProcessing;

/**
 * @author  janis
 */
public class Controller {
	
	/**
	 * @uml.property  name="givenAttributes"
	 * @uml.associationEnd  
	 */
	private Attributes givenAttributes;
	/**
	 * @uml.property  name="graphs"
	 * @uml.associationEnd  
	 */
	private GraphList graphs;

	public void processRequest() {
		
		System.out.println("Starting program..");
		givenAttributes = new InputProcessing().createAttributes();
		System.out.println("Input processed..");
		
		new GraphReducer().reduce(givenAttributes);
		System.out.println("Graphs reduced..");
		
		if (givenAttributes.getMethod()<2) {
			System.out.println("Finding loops..");
			Loops looper = new Loops(givenAttributes);
			graphs = givenAttributes.getGraphs();
			graphs.createIterator();
			
			if (givenAttributes.getMethod()==0) {
				while (graphs.checkNextGraph()) {
					Graph graph = graphs.getNextGraph();
					looper.findLoops(graph);
				}
			} else {
				System.out.println("Given method: Permutations");
				while (graphs.checkNextGraph()) {
					PermutationGraph graph = graphs.getNextPermutationGraph();
					looper.findLoops(graph);
				}
			}
			
			System.out.println("Loops found..");
			new GenerateOutput().generate(givenAttributes);
		} else {
			new GenerateOutput().generateComposite(givenAttributes);
		}
		
	}
}
